"""Fase_3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^Registro/$', "fase3.views.Crear_Usuario"),
    url(r'^Registro/Admin/$', "fase3.views.Crear_Administrador"),
    url(r'^Registro/Help/$', "fase3.views.Crear_Help"),
    url(r'^Admin/Usuario/$', "fase3.views.Admin_Usuario"),
    url(r'^Admin/Productos/$', "fase3.views.Admin_Productos"),
    url(r'^Admin/Categorias/$', "fase3.views.Admin_Categorias"),
    url(r'^Eliminar/Usuario/(?P<ids>\d+)$', "fase3.views.Eliminar_Usuario"),
    url(r'^Eliminar/Producto/(?P<pro>\w+)$', "fase3.views.Eliminar_Producto2"),
    url(r'^Eliminar/Comentario/(?P<pro>\w+)/(?P<ids>\d+)$', "fase3.views.Eliminar_Comentario"),
    url(r'^Editar/Producto/(?P<pro>\w+)$', "fase3.views.Editar_Producto2"),
    url(r'^Editar/Categoria/(?P<cat>\w+)$', "fase3.views.Editar_Categoria"),
    url(r'^Eliminar/Carrito/(?P<id>\d+)/(?P<pro>\w+)$', "fase3.views.EliminarProducto_Carrito2"),
    url(r'^Eliminar/Categoria/(?P<cat>\w+)$', "fase3.views.Eliminar_Categoria"),
    url(r'^Editar/Usuario/(?P<id>\d+)$', "fase3.views.Editar_Usuario2"),
    url(r'^Editar/Carrito/(?P<id>\d+)$', "fase3.views.Carrito_Usuario"),
    url(r'^Login/$', "fase3.views.Login"),
    url(r'^Bitacora/$', "fase3.views.Bitacoras"),
    url(r'^Categoria/Crear/$', "fase3.views.Crear_Categoria"),
    url(r'^Producto/Crear/$', "fase3.views.Crear_Producto"),
    url(r'^Producto/Carga_Masiva/$', "fase3.views.Carga_Masiva"),
    url(r'^Aprobar/(?P<Seg>\d+)/(?P<Dia>\d+)/(?P<ID>\d+)/(?P<Min>\d+)/(?P<Hor>\d+)$', "fase3.views.Aprobar_Cuenta"),
    url(r'^Producto/Categoria/(?P<cat>\w+)$', "fase3.views.CategoriaEspecifica"),
    url(r'^Producto/Categoria/(?P<cat>\w+)/(?P<orden>\w+)$', "fase3.views.CategoriaEspecifica2"),
    url(r'^Producto/(?P<pro>\w+)$', "fase3.views.Producto_Invidual"),
    url(r'^Eliminar/(?P<pro>\w+)$', "fase3.views.EliminarProducto_Carrito"),
    url(r'^Productos/(?P<orden>\w+)$', "fase3.views.ProductosTodos2"),
    url(r'^Producto/', "fase3.views.ProductosTodos"),
    url(r'^Logout/', "fase3.views.Logout"),
    url(r'^Compras/', "fase3.views.Compras"),
    url(r'^REP2/', "fase3.views.Reporte_2"),
    url(r'^REP3/', "fase3.views.Reporte_3"),
    url(r'^REP4/', "fase3.views.Rep4"),
    url(r'^REP5/', "fase3.views.Rep5"),
    url(r'^REP6/', "fase3.views.Rep6"),
    url(r'^REP7/', "fase3.views.Rep7"),
    url(r'^REP8/', "fase3.views.Rep8"),
    url(r'^REP9/', "fase3.views.Reporte_9"),
    url(r'^REP10/', "fase3.views.Reporte_10"),
    url(r'^Perfil/', "fase3.views.Actualizar_Usuario"),
    url(r'^Inicio/', "fase3.views.Inicio"),
    url(r'^Datos/', "fase3.views.Editar_Datos"),
    url(r'^Mis_Productos/', "fase3.views.Mis_Productos"),
    url(r'^Misproductos/(?P<cat>\w+)$', "fase3.views.Mis_ProductosCategoria"),
    url(r'^Misproductos/Editar/(?P<pro>\w+)$', "fase3.views.Editar_Producto"),
    url(r'^Busqueda/(?P<pro>\w+)/(?P<cat>\w+)$', "fase3.views.Algo"),
]
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)