from django.db import models
import time
import datetime
# Create your models here.
from django.db import models


class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(unique=True, max_length=160, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auth_group'





class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    password = models.CharField(max_length=256, blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=60, blank=True, null=True)
    first_name = models.CharField(max_length=60, blank=True, null=True)
    last_name = models.CharField(max_length=60, blank=True, null=True)
    email = models.CharField(max_length=508, blank=True, null=True)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'

    @classmethod
    def Ingresar_Usuario2(self,ids,nombre,apellido,passw,correo):
        fecha_actual=str("20")+time.strftime("%y-%m-%d")
        self.objects.create(username=ids,date_joined=fecha_actual,id=ids,first_name=nombre,last_name=apellido,email=correo,password=passw,is_superuser=False,is_staff=True,is_active=True)
        return self
    
    @classmethod
    def Ingresar_Usuario3(self,ids,nombre,apellido,passw,correo):
        fecha_actual=str("20")+time.strftime("%y-%m-%d")
        self.objects.create(username=ids,date_joined=fecha_actual,id=ids,first_name=nombre,last_name=apellido,email=correo,password=passw,is_superuser=True,is_staff=True,is_active=True)
        return self




class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=400, blank=True, null=True)
    action_flag = models.IntegerField()
    change_message = models.TextField(blank=True, null=True)
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    user = models.ForeignKey(AuthUser)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app_label = models.CharField(max_length=200, blank=True, null=True)
    model = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=510, blank=True, null=True)
    name = models.CharField(max_length=510, blank=True, null=True)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'

class ExampleModel(models.Model):
    model_pic = models.ImageField(upload_to = 'pic_folder/', default = 'pic_folder/None/no-img.jpg')


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=80)
    session_data = models.TextField(blank=True, null=True)
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Usuario(models.Model):
    id_usuario = models.BigIntegerField(primary_key=True)
    nombre_usuario = models.CharField(max_length=240, blank=True, null=True)
    apellido_usuario = models.CharField(max_length=320, blank=True, null=True)
    password_usuario = models.CharField(max_length=100, blank=True, null=True)
    correo = models.CharField(max_length=120)
    telefono_usuario = models.BigIntegerField(blank=True, null=True)
    fotografia = models.CharField(max_length=100, blank=True, null=True)
    genero = models.CharField(max_length=4, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    fecha_creacion = models.DateField(blank=True, null=True)
    direccion_usuario = models.CharField(max_length=400, blank=True, null=True)
    credito = models.FloatField()
    ganancia_usuario = models.FloatField(blank=True, null=True)
    clase_cliente = models.CharField(max_length=12, blank=True, null=True)


    def __unicode__(self):
        return str(self.id_usuario)

    class Meta:
        managed = False
        db_table = 'usuario'

    @classmethod
    def Ingresar_Usuario(self,ids,nombre,apellido,passw,correo,telefono,foto,genero,fechan,dire):
        fecha_actual=str("20")+time.strftime("%y-%m-%d")
        self.objects.create(id_usuario=ids,apellido_usuario=apellido,nombre_usuario=nombre,password_usuario=passw,correo=correo,telefono_usuario=telefono,fotografia=foto,genero=genero,fecha_nacimiento=fechan,direccion_usuario=dire,credito=0,clase_cliente=0,ganancia_usuario=0,fecha_creacion=fecha_actual)
        return self

class Categoria(models.Model):
    nombre_categoria = models.CharField(primary_key=True, max_length=40)
    categoria_padre = models.ForeignKey('self', db_column='categoria_padre', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'categoria'

    def __unicode__(self):
        return str(self.nombre_categoria).encode('utf-8')

class Color(models.Model):
    nombre_color = models.CharField(primary_key=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'color'

    def __unicode__(self):
        return str(self.nombre_color)

class Producto(models.Model):
    codigo_producto = models.CharField(primary_key=True, max_length=80)
    imagen = models.CharField(max_length=120, blank=True, null=True)
    cantidad = models.BigIntegerField()
    descripcion = models.CharField(max_length=1000, blank=True, null=True)
    precio_producto = models.FloatField()
    fecha_publicacion = models.DateField(blank=True, null=True)
    nombre_producto = models.CharField(max_length=400)
    categoria_nombre_categoria = models.ForeignKey(Categoria, db_column='categoria_nombre_categoria')

    class Meta:
        managed = False
        db_table = 'producto'

    def __unicode__(self):
        return str(self.codigo_producto)

    @classmethod
    def Ingresar_Producto(self,cod,nombre,img,cant,descrip,precio,categoria):
        fecha_actual=str("20")+time.strftime("%y-%m-%d")
        self.objects.create(categoria_nombre_categoria=categoria,nombre_producto=nombre,codigo_producto=cod,imagen=img,cantidad=cant,descripcion=descrip,precio_producto=precio,fecha_publicacion=fecha_actual)
        return self

class DetalleProducto(models.Model):
    propietario = models.ForeignKey('Usuario', db_column='propietario')
    codigo_pd = models.ForeignKey('Producto', db_column='codigo_pd', primary_key=True)
    puntuacion = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'detalle_producto'

    @classmethod

    def Ingresar_DetalleProducto(self, propietario, producto):
        self.objects.create(propietario=propietario,codigo_pd=producto,puntuacion=0)
        return self

class ComentarioProducto(models.Model):
    producto = models.ForeignKey('Producto', db_column='producto')
    id_comentaroi = models.BigIntegerField(primary_key=True)
    comentario = models.CharField(max_length=4000, blank=True, null=True)
    autor = models.CharField(max_length=560)
    puntuacion = models.BigIntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comentario_producto'

    @classmethod
    def Ingresar_Comentario(self,cod,autor,pro,coment,punt):
        fecha_actual=str("20")+time.strftime("%y-%m-%d")
        self.objects.create(fecha=fecha_actual,producto=pro,id_comentaroi=cod,autor=autor,comentario=coment,puntuacion=punt)
        return self

class Carrito(models.Model):
    usuario_id_usuario = models.ForeignKey('Usuario', db_column='usuario_id_usuario', primary_key=True)
    total_carrito = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'carrito'

    @classmethod
    def Crear_Carrito(self, usuario):
        self.objects.create(usuario_id_usuario=usuario,total_carrito=0)
        return self

class DetalleCarrito(models.Model):
    usuario_carrito_detalle = models.ForeignKey(Carrito, db_column='usuario_carrito_detalle')
    producto_carrito_detalle = models.ForeignKey('Producto', db_column='producto_carrito_detalle')
    cantidad = models.BigIntegerField(blank=True, null=True)
    precio_p = models.FloatField(blank=True, null=True)
    id_carritodetalle = models.BigIntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'detalle_carrito'


    @classmethod
    def Ingresar_Carrito(self, id_detalle, cantidad , autor, pro, precio):
        self.objects.create(id_carritodetalle=id_detalle,producto_carrito_detalle=pro,cantidad=cantidad,precio_p=precio,usuario_carrito_detalle=autor)
        return self

    def __unicode__(self):
        return str(self.id_carritodetalle)
class Factura(models.Model):
    id_factura = models.BigIntegerField(primary_key=True)
    fecha_factura = models.DateField(blank=True, null=True)
    carrito_usuario_id_usuario = models.ForeignKey(Carrito, db_column='carrito_usuario_id_usuario')
    total_factura = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'factura'

    @classmethod
    def Ingresar_Factura(self, id, carrito , total):
        self.objects.create(id_factura=id,carrito_usuario_id_usuario=carrito,total_factura=total,fecha_factura=datetime.datetime.now())
        return self

class DetalleFactura(models.Model):
    id_detalle_factura = models.BigIntegerField(primary_key=True)
    cantidad_factura = models.BigIntegerField(blank=True, null=True)
    precio_unidad = models.FloatField(blank=True, null=True)
    factura_id_factura = models.ForeignKey('Factura', db_column='factura_id_factura')
    producto_codigo_producto = models.ForeignKey('Producto', db_column='producto_codigo_producto')

    class Meta:
        managed = False
        db_table = 'detalle_factura'

    @classmethod

    def Ingresar_DetalleFactura(self, id, cantidad , precio, factura ,producto):
        self.objects.create(id_detalle_factura=id,cantidad_factura=cantidad,precio_unidad=precio,factura_id_factura=factura,producto_codigo_producto=producto)
        return self

class ColorProducto(models.Model):
    cod_producto = models.ForeignKey('Producto', db_column='cod_producto')
    nombre_cp = models.ForeignKey(Color, db_column='nombre_cp')
    id_colorproducto = models.BigIntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'color_producto'

class Bitacora(models.Model):
    id_bitacora = models.BigIntegerField(primary_key=True)
    accion = models.CharField(max_length=30, blank=True, null=True)
    descripcion = models.CharField(max_length=1000, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    usuario_bitacora = models.ForeignKey('Usuario', db_column='usuario_bitacora')

    class Meta:
        managed = False
        db_table = 'bitacora'

    @classmethod

    def Ingresar_Bitacora(self,cod,usr,tipo,desc):
        fecha_actual=str("20")+time.strftime("%y-%m-%d")
        self.objects.create(id_bitacora=cod,accion=tipo,descripcion=desc,usuario_bitacora=usr,fecha=fecha_actual)
        return self

class Datos(models.Model):
    id_datos = models.BigIntegerField(primary_key=True)
    mision = models.CharField(max_length=2000, blank=True, null=True)
    vision = models.CharField(max_length=500, blank=True, null=True)
    acerca_de = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'datos'














