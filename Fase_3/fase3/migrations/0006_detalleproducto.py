# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0005_producto'),
    ]

    operations = [
        migrations.CreateModel(
            name='DetalleProducto',
            fields=[
                ('codigo_pd', models.ForeignKey(primary_key=True, db_column=b'codigo_pd', serialize=False, to='fase3.Producto')),
                ('puntuacion', models.FloatField(null=True, blank=True)),
            ],
            options={
                'db_table': 'detalle_producto',
                'managed': False,
            },
        ),
    ]
