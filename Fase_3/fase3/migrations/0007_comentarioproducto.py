# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0006_detalleproducto'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComentarioProducto',
            fields=[
                ('id_comentaroi', models.BigIntegerField(serialize=False, primary_key=True)),
                ('comentario', models.CharField(max_length=4000, null=True, blank=True)),
                ('autor', models.CharField(max_length=560)),
            ],
            options={
                'db_table': 'comentario_producto',
                'managed': False,
            },
        ),
    ]
