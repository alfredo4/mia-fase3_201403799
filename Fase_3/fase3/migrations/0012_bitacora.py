# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0011_colorproducto'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bitacora',
            fields=[
                ('id_bitacora', models.BigIntegerField(serialize=False, primary_key=True)),
                ('accion', models.CharField(max_length=30, null=True, blank=True)),
                ('descripcion', models.CharField(max_length=1000, null=True, blank=True)),
                ('fecha', models.DateField(null=True, blank=True)),
            ],
            options={
                'db_table': 'bitacora',
                'managed': False,
            },
        ),
    ]
