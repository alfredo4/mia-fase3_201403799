# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0002_examplemodel'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('nombre_categoria', models.CharField(max_length=40, serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'categoria',
                'managed': False,
            },
        ),
    ]
