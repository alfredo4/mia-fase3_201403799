# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0007_comentarioproducto'),
    ]

    operations = [
        migrations.CreateModel(
            name='Carrito',
            fields=[
                ('usuario_id_usuario', models.ForeignKey(primary_key=True, db_column=b'usuario_id_usuario', serialize=False, to='fase3.Usuario')),
                ('total_carrito', models.FloatField(null=True, blank=True)),
            ],
            options={
                'db_table': 'carrito',
                'managed': False,
            },
        ),
    ]
