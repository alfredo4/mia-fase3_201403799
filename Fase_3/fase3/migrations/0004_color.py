# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0003_categoria'),
    ]

    operations = [
        migrations.CreateModel(
            name='Color',
            fields=[
                ('nombre_color', models.CharField(max_length=80, serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'color',
                'managed': False,
            },
        ),
    ]
