# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0012_bitacora'),
    ]

    operations = [
        migrations.CreateModel(
            name='Datos',
            fields=[
                ('id_datos', models.BigIntegerField(serialize=False, primary_key=True)),
                ('mision', models.CharField(max_length=2000, null=True, blank=True)),
                ('vision', models.CharField(max_length=500, null=True, blank=True)),
                ('acerca_de', models.CharField(max_length=500, null=True, blank=True)),
            ],
            options={
                'db_table': 'datos',
                'managed': False,
            },
        ),
    ]
