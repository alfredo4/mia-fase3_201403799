# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0008_carrito'),
    ]

    operations = [
        migrations.CreateModel(
            name='DetalleCarrito',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cantidad', models.BigIntegerField(null=True, blank=True)),
                ('precio_p', models.FloatField(null=True, blank=True)),
            ],
            options={
                'db_table': 'detalle_carrito',
                'managed': False,
            },
        ),
    ]
