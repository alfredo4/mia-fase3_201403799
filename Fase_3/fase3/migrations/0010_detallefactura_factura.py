# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0009_detallecarrito'),
    ]

    operations = [
        migrations.CreateModel(
            name='DetalleFactura',
            fields=[
                ('id_detalle_factura', models.BigIntegerField(serialize=False, primary_key=True)),
                ('cantidad_factura', models.BigIntegerField(null=True, blank=True)),
                ('precio_unidad', models.FloatField(null=True, blank=True)),
            ],
            options={
                'db_table': 'detalle_factura',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Factura',
            fields=[
                ('id_factura', models.BigIntegerField(serialize=False, primary_key=True)),
                ('fecha_factura', models.DateField(null=True, blank=True)),
            ],
            options={
                'db_table': 'factura',
                'managed': False,
            },
        ),
    ]
