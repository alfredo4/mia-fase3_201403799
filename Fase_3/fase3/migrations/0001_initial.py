# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AuthGroup',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=160, unique=True, null=True, blank=True)),
            ],
            options={
                'db_table': 'auth_group',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AuthUser',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('password', models.CharField(max_length=256, null=True, blank=True)),
                ('last_login', models.DateTimeField(null=True, blank=True)),
                ('is_superuser', models.BooleanField()),
                ('username', models.CharField(max_length=60, unique=True, null=True, blank=True)),
                ('first_name', models.CharField(max_length=60, null=True, blank=True)),
                ('last_name', models.CharField(max_length=60, null=True, blank=True)),
                ('email', models.CharField(max_length=508, null=True, blank=True)),
                ('is_staff', models.BooleanField()),
                ('is_active', models.BooleanField()),
                ('date_joined', models.DateTimeField()),
            ],
            options={
                'db_table': 'auth_user',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoAdminLog',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('action_time', models.DateTimeField()),
                ('object_id', models.TextField(null=True, blank=True)),
                ('object_repr', models.CharField(max_length=400, null=True, blank=True)),
                ('action_flag', models.IntegerField()),
                ('change_message', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'django_admin_log',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoContentType',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('app_label', models.CharField(max_length=200, null=True, blank=True)),
                ('model', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'django_content_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoMigrations',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('app', models.CharField(max_length=510, null=True, blank=True)),
                ('name', models.CharField(max_length=510, null=True, blank=True)),
                ('applied', models.DateTimeField()),
            ],
            options={
                'db_table': 'django_migrations',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoSession',
            fields=[
                ('session_key', models.CharField(max_length=80, serialize=False, primary_key=True)),
                ('session_data', models.TextField(null=True, blank=True)),
                ('expire_date', models.DateTimeField()),
            ],
            options={
                'db_table': 'django_session',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id_usuario', models.BigIntegerField(serialize=False, primary_key=True)),
                ('nombre_usuario', models.CharField(max_length=240, null=True, blank=True)),
                ('apellido_usuario', models.CharField(max_length=320, null=True, blank=True)),
                ('password_usuario', models.CharField(max_length=100, null=True, blank=True)),
                ('correo', models.CharField(max_length=120)),
                ('telefono_usuario', models.BigIntegerField(null=True, blank=True)),
                ('fotografia', models.CharField(max_length=100, null=True, blank=True)),
                ('genero', models.CharField(max_length=4, null=True, blank=True)),
                ('fecha_nacimiento', models.DateField(null=True, blank=True)),
                ('fecha_creacion', models.DateField(null=True, blank=True)),
                ('direccion_usuario', models.CharField(max_length=400, null=True, blank=True)),
                ('credito', models.FloatField()),
                ('ganancia_usuario', models.FloatField(null=True, blank=True)),
                ('clase_cliente', models.CharField(max_length=12, null=True, blank=True)),
            ],
            options={
                'db_table': 'usuario',
                'managed': False,
            },
        ),
    ]
