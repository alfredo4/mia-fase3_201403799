# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0004_color'),
    ]

    operations = [
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('codigo_producto', models.CharField(max_length=80, serialize=False, primary_key=True)),
                ('imagen', models.CharField(max_length=120, null=True, blank=True)),
                ('cantidad', models.BigIntegerField()),
                ('descripcion', models.CharField(max_length=1000, null=True, blank=True)),
                ('precio_producto', models.FloatField()),
                ('fecha_publicacion', models.DateField(null=True, blank=True)),
                ('nombre_producto', models.CharField(max_length=400)),
            ],
            options={
                'db_table': 'producto',
                'managed': False,
            },
        ),
    ]
