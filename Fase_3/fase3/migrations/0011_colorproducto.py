# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fase3', '0010_detallefactura_factura'),
    ]

    operations = [
        migrations.CreateModel(
            name='ColorProducto',
            fields=[
                ('id_colorproducto', models.BigIntegerField(serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'color_producto',
                'managed': False,
            },
        ),
    ]
