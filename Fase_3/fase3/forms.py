from django import forms
from django.db import models
from django.shortcuts import get_object_or_404
from .models import Usuario,Categoria,Producto

class CategoriaForm(forms.ModelForm):
    class Meta:
    	model = Categoria
    	fields =[
    		"nombre_categoria",
    		"categoria_padre",
    	]

class EditarUForm(forms.ModelForm):
    class Meta:
    	model = Usuario
    	fields =[
    		"nombre_usuario",
    		"apellido_usuario",
    		"password_usuario",
    		"correo",
    		"telefono_usuario",
    		"fotografia",
    		"genero",
    		"fecha_nacimiento",
    		"direccion_usuario",
    		"ganancia_usuario",
    		"credito",
    		"clase_cliente",
    	]

class EditarProducto2(forms.ModelForm):
    class Meta:
    	model = Producto
    	fields =[
    		"nombre_producto",
    		"categoria_nombre_categoria",
    		"cantidad",
    		"precio_producto",
    		"fecha_publicacion",
    		"imagen",
    		"descripcion",
    	]


class UsuariosForm(forms.Form):
	Nombre=forms.CharField(max_length=240)
	Apellido = forms.CharField(max_length=320)
	Correo=forms.EmailField()
	Clave=forms.CharField(widget=forms.PasswordInput,max_length=100,min_length=8)
	Confirmar_Clave = forms.CharField(widget=forms.PasswordInput,max_length=100,min_length=8)
	Direccion = forms.CharField(max_length=400)
	Telefono = forms.IntegerField()
	Genero=forms.CharField(max_length=4)
	Fecha_Nacimiento = forms.DateField()
	Foto = forms.FileField()


class UsuariosConfiguracionForm(forms.Form):
	Nombre=forms.CharField(max_length=240)
	Apellido = forms.CharField(max_length=320)
	Correo=forms.EmailField(widget=forms.TextInput(attrs={'size': '30'}))
	Clave=forms.CharField(widget=forms.PasswordInput(attrs={ 'required': False }),max_length=100,min_length=8)
	Direccion = forms.CharField(max_length=400)
	Telefono = forms.IntegerField()

class ProductosForm(forms.Form):
	Codigo_Producto=forms.CharField(max_length=80)
	Nombre_Producto= forms.CharField(max_length=400)
	Categoria=forms.ModelChoiceField(queryset=Categoria.objects.all().exclude(categoria_padre = None))
	Precio=forms.FloatField()
	Cantidad=forms.IntegerField()
	Descripcion=forms.CharField(widget=forms.Textarea(attrs={ 'class':'form-control', 'rows':'5' }),max_length=1000,required=False)
	Imagen = forms.FileField()

class ProductoEditarForm(forms.Form):
	Nombre_Producto= forms.CharField(max_length=400)
	Categoria=forms.ModelChoiceField(queryset=Categoria.objects.all().exclude(categoria_padre = None))
	Precio=forms.FloatField()
	Cantidad=forms.IntegerField()
	Descripcion=forms.CharField(widget=forms.Textarea(attrs={ 'class':'form-control', 'rows':'5' }),max_length=1000,required=False)

class DatosForm(forms.Form):
	Mision=forms.CharField(widget=forms.Textarea(attrs={ 'class':'form-control', 'rows':'5' }))
	Acerca_De=forms.CharField(widget=forms.Textarea(attrs={ 'class':'form-control', 'rows':'5' }))
	Vision=forms.CharField(widget=forms.Textarea(attrs={ 'class':'form-control', 'rows':'5' }))

class LoginForm(forms.Form):
	ID=forms.IntegerField()
	Password=forms.CharField(widget=forms.PasswordInput,max_length=100,min_length=8)

class ComentForm(forms.Form):
	Comentario=  forms.CharField(widget=forms.Textarea(attrs={ 'class':'form-control', 'rows':'5' }))
	Puntuacion=  forms.IntegerField(min_value=0,max_value=5)

class ComprarForm(forms.Form):
	Cantidad=  forms.IntegerField(widget=forms.NumberInput(attrs={ 'class':'form-control','required': True }))

class CompraCarritoForm(forms.Form):
	Password=  forms.CharField(widget=forms.PasswordInput(attrs={ 'class':'form-control','required': True }))

class BuscarForm(forms.Form):
	Producto=  forms.CharField(widget=forms.TextInput(attrs={ 'class':'form-control','required': True }))
	Categoria=forms.ModelChoiceField(required=False,queryset=Categoria.objects.all().exclude(categoria_padre = None))

class CargaMasivaForm(forms.Form):
	Arhivo_CSV = forms.FileField()

class Reporte3Form(forms.Form):

	Fecha= forms.DateField()

class Reporte9Form(forms.Form):

	Fecha= forms.DateField()

class Reporte10Form(forms.Form):

	Cantidad= forms.IntegerField()
