from django.contrib import admin
from .models import Datos,Usuario,Bitacora,Categoria,Color,Producto,DetalleProducto,ComentarioProducto, Carrito,DetalleCarrito

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Categoria)
admin.site.register(Color)
admin.site.register(Producto)
admin.site.register(DetalleProducto)
admin.site.register(ComentarioProducto)
admin.site.register(Carrito)
admin.site.register(DetalleCarrito)
admin.site.register(Bitacora)
admin.site.register(Datos)