from django.shortcuts import render
from django.contrib.auth import logout
import random
import shutil
import datetime
import time
from io import BytesIO
from django.db import connection
from django.http import HttpResponse
from django.shortcuts import render,get_object_or_404, redirect
from .models import Usuario,Datos, Bitacora,Color, ColorProducto, DetalleFactura, Factura, ExampleModel,Carrito, Producto, DetalleCarrito, AuthUser, Categoria, DetalleProducto, ComentarioProducto
from .forms import UsuariosForm,DatosForm, Reporte3Form, Reporte10Form, Reporte9Form,CargaMasivaForm, EditarProducto2, EditarUForm, BuscarForm,ProductoEditarForm, UsuariosConfiguracionForm, LoginForm, CompraCarritoForm, CategoriaForm, ProductosForm, ComentForm, ComprarForm
from collections import namedtuple
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
import os
from cStringIO import StringIO
from reportlab.pdfgen import canvas
from django.http import HttpResponse
import reportlab
from django.http import HttpResponse
from django.views.generic import ListView
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table

# Create your views here.
def Crear_Help(request):
	#rawCursor = connection.connection.cursor()
    form=UsuariosForm(request.POST or None,request.FILES or None)
    cursor=connection.cursor()
    if form.is_valid():
    	ids=0
    	mayuscula=0
    	numero=0
        clave=form.cleaned_data['Clave']
        confirmar=form.cleaned_data['Confirmar_Clave']
        for letra in clave:
        	if letra.isdigit():
        		numero=1
        	if letra.isupper():
        		mayuscula=1
        if clave != confirmar or mayuscula==0 or numero==0 or clave.isalpha():
        	context_data = {"error":'A',"form" : form,"Accion": "Guardar","titulo":"Registro de Usuario"}
        	return render(request, "form.html", context_data)
        else:
        	cursor.execute("SELECT seq_usuarios_idusuario.nextval FROM dual")
        	rows = cursor.fetchall()
        	for obj in rows:
        		ids=obj[0]
        	nombre_usuario=form.cleaned_data['Nombre']
        	correo=form.cleaned_data['Correo']
        	tele=form.cleaned_data['Telefono']
       		apellido_usuario=form.cleaned_data['Apellido']
      		direccion_usuario=form.cleaned_data['Direccion']
        	genero=form.cleaned_data['Genero']
        	fecha=form.cleaned_data['Fecha_Nacimiento']
        	nombre_imagen=str(ids)+str(".png")
        	clave=random.randint(10000000, 99999999)
    		handle_uploaded_file(request.FILES['Foto'],nombre_imagen)
    		Usuario.Ingresar_Usuario(ids,nombre_usuario,apellido_usuario,clave,correo,tele,nombre_imagen,genero,fecha,direccion_usuario)
    		AuthUser.Ingresar_Usuario3(ids,nombre_usuario,apellido_usuario,clave,correo)
    		instance=get_object_or_404(Usuario,id_usuario=ids)
    		instance.clase_cliente='H'
    		u = User.objects.get(username=ids)
    		u.set_password(instance.password_usuario)
    		u.save()
    		instance.save()
    		Carrito.Crear_Carrito(instance)
    		send_mail(
    			'Credenciales Help Desk',
    			'Credenciales:\nID: '+str(ids)+"\nPassword: "+str(clave),
    			'admin@fase3.com',
    			[str(correo)],
    			fail_silently=False,
				)
        	return redirect("/Login/")
    context_data = {"form" : form,"Accion": "Guardar","titulo":"Nuevo Help Desk"}
    return render(request, "form.html", context_data)
def Crear_Administrador(request):
	#rawCursor = connection.connection.cursor()
    form=UsuariosForm(request.POST or None,request.FILES or None)
    cursor=connection.cursor()
    if form.is_valid():
    	ids=0
    	mayuscula=0
    	numero=0
        clave=form.cleaned_data['Clave']
        confirmar=form.cleaned_data['Confirmar_Clave']
        for letra in clave:
        	if letra.isdigit():
        		numero=1
        	if letra.isupper():
        		mayuscula=1
        if clave != confirmar or mayuscula==0 or numero==0 or clave.isalpha():
        	context_data = {"error":'A',"form" : form,"Accion": "Guardar","titulo":"Registro de Usuario"}
        	return render(request, "form.html", context_data)
        else:
        	cursor.execute("SELECT seq_usuarios_idusuario.nextval FROM dual")
        	rows = cursor.fetchall()
        	for obj in rows:
        		ids=obj[0]
        	nombre_usuario=form.cleaned_data['Nombre']
        	correo=form.cleaned_data['Correo']
        	tele=form.cleaned_data['Telefono']
       		apellido_usuario=form.cleaned_data['Apellido']
      		direccion_usuario=form.cleaned_data['Direccion']
        	genero=form.cleaned_data['Genero']
        	fecha=form.cleaned_data['Fecha_Nacimiento']
        	nombre_imagen=str(ids)+str(".png")
        	clave=random.randint(10000000, 99999999)
    		handle_uploaded_file(request.FILES['Foto'],nombre_imagen)
    		Usuario.Ingresar_Usuario(ids,nombre_usuario,apellido_usuario,clave,correo,tele,nombre_imagen,genero,fecha,direccion_usuario)
    		AuthUser.Ingresar_Usuario3(ids,nombre_usuario,apellido_usuario,clave,correo)
    		instance=get_object_or_404(Usuario,id_usuario=ids)
    		instance.clase_cliente='S'
    		u = User.objects.get(username=ids)
    		u.set_password(instance.password_usuario)
    		u.save()
    		instance.save()
    		Carrito.Crear_Carrito(instance)
    		send_mail(
    			'Credenciales Administrador',
    			'Credenciales:\nID: '+str(ids)+"\nPassword: "+str(clave),
    			'admin@fase3.com',
    			[str(correo)],
    			fail_silently=False,
				)
        	return redirect("/Login/")
    context_data = {"form" : form,"Accion": "Guardar","titulo":"Nuevo Administrador"}
    return render(request, "form.html", context_data)
def Crear_Usuario(request):
	#rawCursor = connection.connection.cursor()
    form=UsuariosForm(request.POST or None,request.FILES or None)
    cursor=connection.cursor()
    if form.is_valid():
    	ids=0
    	mayuscula=0
    	numero=0
        clave=form.cleaned_data['Clave']
        confirmar=form.cleaned_data['Confirmar_Clave']
        for letra in clave:
        	if letra.isdigit():
        		numero=1
        	if letra.isupper():
        		mayuscula=1
        if clave != confirmar or mayuscula==0 or numero==0 or clave.isalpha():
        	context_data = {"error":'A',"form" : form,"Accion": "Guardar","titulo":"Registro de Usuario"}
        	return render(request, "form.html", context_data)
        else:
        	cursor.execute("SELECT seq_usuarios_idusuario.nextval FROM dual")
        	rows = cursor.fetchall()
        	for obj in rows:
        		ids=obj[0]
        	nombre_usuario=form.cleaned_data['Nombre']
        	correo=form.cleaned_data['Correo']
        	tele=form.cleaned_data['Telefono']
       		apellido_usuario=form.cleaned_data['Apellido']
      		direccion_usuario=form.cleaned_data['Direccion']
        	genero=form.cleaned_data['Genero']
        	fecha=form.cleaned_data['Fecha_Nacimiento']
        	nombre_imagen=str(ids)+str(".png")
    		handle_uploaded_file(request.FILES['Foto'],nombre_imagen)
    		Usuario.Ingresar_Usuario(ids,nombre_usuario,apellido_usuario,clave,correo,tele,nombre_imagen,genero,fecha,direccion_usuario)
    		AuthUser.Ingresar_Usuario2(ids,nombre_usuario,apellido_usuario,clave,correo)
    		send_mail(
    			'Verificacion de Cuenta',
    			'Ingrese al siguiente link para verificar su cuenta:\n' 
    			+'http://192.168.0.8:8000/Aprobar/'+time.strftime("%S")+"/"+time.strftime("%d")+"/"+str(ids)+"/"+time.strftime("%M")+"/"+time.strftime("%H"),
    			'admin@fase3.com',
    			[str(correo)],
    			fail_silently=False,
				)
    		context_data = {"error":'B',"form" : form,"Accion": "Guardar","titulo":"Registro de Usuario"}
        	return render(request, "form.html", context_data)
    context_data = {"form" : form,"Accion": "Guardar","titulo":"Registro de Usuario"}
    return render(request, "form.html", context_data)

def Actualizar_Usuario(request):
	if not request.user.is_staff:
		return render(request, "404.html")
	usuario=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	form=UsuariosConfiguracionForm(request.POST or None,initial={'Nombre': usuario.nombre_usuario,'Apellido': usuario.apellido_usuario,'Correo':usuario.correo,'Clave':usuario.password_usuario,'Direccion':usuario.direccion_usuario,'Telefono':usuario.telefono_usuario})
	if form.is_valid():
		nombre_usuario=form.cleaned_data['Nombre']
		correo=form.cleaned_data['Correo']
		tele=form.cleaned_data['Telefono']
		apellido_usuario=form.cleaned_data['Apellido']
		direccion_usuario=form.cleaned_data['Direccion']
		clave=form.cleaned_data['Clave']
		usuario.nombre_usuario=nombre_usuario
		usuario.correo=correo
		usuario.telefono_usuario=tele
		usuario.apellido_usuario=apellido_usuario
		usuario.direccion_usuario=direccion_usuario
		u = User.objects.get(username=request.user.get_username)
		u2=get_object_or_404(AuthUser,username=request.user.get_username)
		u2.first_name=nombre_usuario
		u2.last_name=apellido_usuario
		numero=0
		mayuscula=0
		if clave is None:
			u.save()
			usuario.save()
			return redirect('/Perfil/')
		else:
			for letra in clave:
				if letra.isdigit():
					numero=1
        		if letra.isupper():
        			mayuscula=1
        	print(numero)
        	print(mayuscula)
        	if  numero==0 or clave.isalpha():
        		context_data = {"form" : form,"Accion": "Guardar","titulo":"Mi Perfil","error":'O'}
        		return render(request, "form.html", context_data)
        	else:
        		usuario.password_usuario=clave
        		u.set_password(clave)
        		u.save()
        		usuario.save()
        		u2.save()
        		return redirect('/Perfil/')
	context_data = {"form" : form,"Accion": "Guardar","titulo":"Mi Perfil"}
	return render(request, "form.html", context_data)

def handle_uploaded_file(f,nombre):
    with open("/home/alfredo/Escritorio/Fase_3/media_cdn/"+str(nombre), 'w+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
def Aprobar_Cuenta(request,Seg,Dia,ID,Min,Hor):
	instance=get_object_or_404(Usuario,id_usuario=ID)
	Minunots=int("0"+Min)
	Minunots+=2
	Hora=int("0"+Hor)
	print(Hora)
	print(Minunots)
	hora_actual=int("0"+time.strftime("%H"))
	minutos_actual=int("0"+time.strftime("%M"))
	if Minunots>60:
		Hora=Hora+1
		Minunots-=60
	if Hora!=hora_actual or minutos_actual>Minunots:
		instance.delete()
		return HttpResponse("<h1>Link Vencido!</h1>")
	if instance.clase_cliente != '0':
		return redirect('/Productos/')
	tipo_cliente=random.randrange(5)
	Carrito.Crear_Carrito(instance)
	clase='E'
	credito=1000
	if tipo_cliente==1:
		credito=5000
		clase='D'
	if tipo_cliente==2:
		credito=10000
		clase='C'
	if tipo_cliente==3:
		credito=15000
		clase='B'
	if tipo_cliente==4:
		credito=20000
		clase='A'
	cursor=connection.cursor()
	u = User.objects.get(username=ID)
	u.set_password(instance.password_usuario)
	u.save()
	user = authenticate(username=instance.id_usuario, password=instance.password_usuario)
	if user is not None:
		login(request, user)
	cursor.execute("UPDATE USUARIO set Credito=%s,Clase_Cliente=%s WHERE ID_Usuario=%s",[credito,clase,ID])
	return redirect('/Producto/')

def Logout(request):
	if request.user.is_staff:
		logout(request)
		return redirect('/Producto/')
	else:
		return redirect('/Login/')


def Login(request):
    form=LoginForm(request.POST or None)
    cursor=connection.cursor()
    if form.is_valid():
    	tipo=9
    	username=form.cleaned_data['ID']
    	password=form.cleaned_data['Password']
    	cursor.execute("Select Clase_Cliente FROM USUARIO WHERE Id_Usuario=%s and Password_Usuario=%s",[username,password])
    	rows = cursor.fetchall()
        for obj in rows:
        	tipo=obj[0]
    	if tipo==0 or tipo ==9:
    		 context_data = {"form" : form,"Accion": "Acpetar","titulo":"Login",'error':'S'}
    		 return render(request, "login.html", context_data)
    	u = User.objects.get(username=username)
    	u.set_password(password)
    	u.save()
    	user = authenticate(username=username, password=password)
    	if user is not None:
        	login(request, user)
        else:
        	print("No Existe")
        return redirect("/Producto/")
    context_data = {"form" : form,"Accion": "Acpetar","titulo":"Login",'error':'no'}
    return render(request, "login.html", context_data)

def Crear_Categoria(request):
	if not request.user.is_superuser:
		return render(request, "404.html")
	form=CategoriaForm(request.POST or None)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		return redirect("/Admin/Categorias/")
	context_data = {"form" : form,"Accion": "Guardar","titulo":"Nueva Categoria"}
	return render(request, "form.html", context_data)

def ProductosTodos(request):
	form=BuscarForm(request.POST or None)
	if form.is_valid():
		producto=form.cleaned_data['Producto']
		categoria=form.cleaned_data['Categoria']
		return redirect("/Busqueda/"+str(producto)+"/"+str(categoria))
	CategoriaLista=Categoria.objects.all()
	Lista=Producto.objects.all()
	context_data = {"form":form,"object_list": Lista,"lista_categoria":CategoriaLista,"titulo":"Productos"}
	return render(request, "productos.html", context_data)

def ProductosTodos2(request,orden):
    form=BuscarForm(request.POST or None)
    if form.is_valid():
        producto=form.cleaned_data['Producto']
        categoria=form.cleaned_data['Categoria']
        return redirect("/Busqueda/"+str(producto)+"/"+str(categoria))
    CategoriaLista=Categoria.objects.all()
    Lista=Producto.objects.all()
    if orden =='preciodesc':
        Lista=Producto.objects.order_by('precio_producto')
    if orden=='precioasc':
        Lista=Producto.objects.order_by('-precio_producto')
    if orden=='fehaasc':
        Lista=Producto.objects.order_by('-fecha_publicacion')
    if orden=='fehadesc':
        Lista=Producto.objects.order_by('fecha_publicacion')
    context_data = {"form":form,"object_list": Lista,"lista_categoria":CategoriaLista,"titulo":"Productos"}
    return render(request, "productos.html", context_data)

def Admin_Usuario(request):
	if not request.user.is_superuser:
		return redirect ("/404/")
	usuarios=Usuario.objects.all()
	context_data = {"usuarios": usuarios,"titulo":"Lista de Usuarios"}
	return render(request, "usuariolista.html", context_data)
def Compras(request):
	if not request.user.is_staff:
		return render(request, "404.html")
	id_usuario=request.user.get_username()
	usuario=get_object_or_404(Usuario,id_usuario=id_usuario)
	form=CompraCarritoForm(request.POST or None)
	carrito=get_object_or_404(Carrito,usuario_id_usuario=id_usuario)
	cursor=connection.cursor()
	cursor.execute("Select Codigo_Producto, Nombre_Producto, DETALLE_CARRITO.CANTIDAD,Precio_Producto, (Precio_Producto*DETALLE_CARRITO.CANTIDAD) as Sub_Total FROM PRODUCTO, DETALLE_CARRITO, CARRITO, USUARIO WHERE ID_USUARIO=%s AND USUARIO.ID_USUARIO = CARRITO.USUARIO_ID_USUARIO AND DETALLE_CARRITO.USUARIO_CARRITO_DETALLE=USUARIO.ID_USUARIO AND PRODUCTO.CODIGO_PRODUCTO= DETALLE_CARRITO.PRODUCTO_CARRITO_DETALLE;",[id_usuario])
	rows = cursor.fetchall()
	if form.is_valid():
		password=form.cleaned_data['Password']
		if usuario.password_usuario!= password:
			context_data = {"mensaje":'M',"productos": rows,"form":form,"Total": str(carrito.total_carrito).replace(",","."),"titulo":"Compras Realizadas"}
			return render(request, "carrito.html", context_data)
		elif usuario.credito < carrito.total_carrito:
			context_data = {"mensaje":'T',"productos": rows,"form":form,"Total": str(carrito.total_carrito).replace(",","."),"titulo":"Compras Realizadas"}
			return render(request, "carrito.html", context_data)
		else:
			ids=0
			cuerpo_factura=''
			cuerpo_factura+=str('Factura: ')
			cursor.execute("SELECT secuencia_factura.nextval FROM dual")
        	rows2 = cursor.fetchall()
        	for obj in rows2:
        		ids=obj[0]
        	cuerpo_factura+=str(ids)+str('\n')
        	cuerpo_factura+=str("Cliente: ")+str(usuario.nombre_usuario)+str(" ")+str(usuario.apellido_usuario)+str('\n')
        	cuerpo_factura+=str("Fecha: ")+str(datetime.datetime.now())+str('\n')
        	cuerpo_factura+=str("Carrito: #")+str(usuario.id_usuario)+str('\n')+str('\n')+str('\n')
        	cuerpo_factura+=str("Productos: ")+str('\n')+str('\n')+str('\n')
        	cuerpo_factura+=str("# Producto 	Nombre 	 	   Cantidad 	 	Precio  	 	SubTotal")+str('\n')
        	Factura.Ingresar_Factura(ids,carrito,carrito.total_carrito)
        	factura=get_object_or_404(Factura,id_factura=ids)
        	for row in rows:
        		cuerpo_factura+=str(row[0])+ str(" 	") +str(row[1])	+ str(" 	") +str(row[2])	+ str(" 	         ") +str(row[3])	 + str("         	              ") +str(row[4])	+str('\n')	
        		producto=get_object_or_404(Producto,codigo_producto=row[0])
        		cursor.execute("SELECT secuencia_factura.nextval FROM dual")
        		rows2 = cursor.fetchall()
        		for obj in rows2:
        			ids=obj[0]
        		DetalleFactura.Ingresar_DetalleFactura(ids,row[2],row[3],factura,producto)
        		producto_detalle=get_object_or_404(DetalleProducto,codigo_pd=producto.codigo_producto)
        		print(producto_detalle.propietario.id_usuario)
        		cursor.execute("UPDATE USUARIO SET Ganancia_Usuario=Ganancia_Usuario+%s WHERE ID_USUARIO=%s",[row[2]*row[3],producto_detalle.propietario.id_usuario])
        	cuerpo_factura+=str('\n')+str('\n')+str('\n')+("Total: Q.")+str(carrito.total_carrito).replace(",",".")
        	send_mail(
    		'Factura de Compra',
    		cuerpo_factura,
    		'admin@fase3.com',
    		[str(usuario.correo)],
    		fail_silently=False,)
    		usuario=get_object_or_404(Usuario,id_usuario=id_usuario)
    		usuario.credito-=carrito.total_carrito
    		usuario.save()
    		carrito.total_carrito=0
    		carrito.save()
    		cursor.execute("DELETE  FROM Detalle_Carrito WHERE Usuario_Carrito_Detalle = %s",[id_usuario])
    		return redirect('/Compras/')
	context_data = {"productos": rows,"form":form,"Total": str(carrito.total_carrito).replace(",","."),"titulo":"Compras Realizadas"}
	return render(request, "carrito.html", context_data)

def EliminarProducto_Carrito(request,pro):
	if not request.user.is_staff:
		return render(request, "404.html")
	ids=request.user.username
	producto=get_object_or_404(Producto,codigo_producto=pro)
	carrito=get_object_or_404(Carrito,usuario_id_usuario=ids)
	detalle=get_object_or_404(DetalleCarrito, usuario_carrito_detalle=ids, producto_carrito_detalle=pro)
	producto.cantidad+=detalle.cantidad
	carrito.total_carrito-=(producto.precio_producto*detalle.cantidad)
	producto.save()
	carrito.save()
	detalle.delete()
	return redirect('/Compras/')

def Eliminar_Usuario(request,ids):
	if  not request.user.is_superuser:
		return render(request, "404.html")
	usuario=get_object_or_404(Usuario,id_usuario=ids)
	usuario.delete()
	cursor=connection.cursor()
	cursor.execute("DELETE FROM AUTH_USER WHERE USERNAME=\'"+str(ids)+"\'")
	return redirect('/Admin/Usuario/')

def Mis_Productos(request):
	if not request.user.is_staff or request.user.is_superuser:
		return render(request, "404.html")
	usuario=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	CategoriaLista=Categoria.objects.all()
	cursor=connection.cursor()
	cursor.execute("SELECT Nombre_Producto,Codigo_Producto,categoria_nombre_categoria,imagen,precio_producto FROM PRODUCTO, DETALLE_PRODUCTO, USUARIO WHERE PRODUCTO.CODIGO_PRODUCTO=DETALLE_PRODUCTO.CODIGO_PD AND DETALLE_PRODUCTO.PROPIETARIO=USUARIO.ID_USUARIO AND ID_USUARIO=%s",[usuario.id_usuario])
	rows = cursor.fetchall()
	context_data = {"object_list": rows,"lista_categoria":CategoriaLista,"titulo":"Mis Productos"}
	return render(request, "misproductos.html", context_data)

def CategoriaEspecifica(request,cat):
	form=BuscarForm(request.POST or None)
	if form.is_valid():
		producto=form.cleaned_data['Producto']
		categoria=form.cleaned_data['Categoria']
		return redirect("/Busqueda/"+str(producto)+"/"+str(categoria))
	cursor=connection.cursor()
	cursor.execute("SELECT Nombre_Producto ,Precio_Producto ,Imagen ,Codigo_Producto ,Categoria_Nombre_Categoria  From Producto,Categoria WHERE (Categoria_Padre=%s or Nombre_Categoria=%s) AND Nombre_Categoria=Categoria_Nombre_Categoria",[cat,cat])
	rows = cursor.fetchall()
	CategoriaLista=Categoria.objects.filter(categoria_padre=cat)
	if not CategoriaLista:
		categoria_padre=get_object_or_404(Categoria,categoria=cat)
		CategoriaLista=Categoria.objects.filter(categoria_padre=categoria_padre.nombre_categoria)
	Lista=rows
	context_data = {"cat":cat,"form":form,"object_list": Lista,"lista_categoria":CategoriaLista,"titulo":cat}
	return render(request, "producto2.html", context_data)

def CategoriaEspecifica2(request,cat,orden):
    form=BuscarForm(request.POST or None)
    if form.is_valid():
        producto=form.cleaned_data['Producto']
        categoria=form.cleaned_data['Categoria']
        return redirect("/Busqueda/"+str(producto)+"/"+str(categoria))
    cursor=connection.cursor()
    cursor.execute("SELECT Nombre_Producto ,Precio_Producto ,Imagen ,Codigo_Producto ,Categoria_Nombre_Categoria  From Producto,Categoria WHERE (Categoria_Padre=%s or Nombre_Categoria=%s) AND Nombre_Categoria=Categoria_Nombre_Categoria",[cat,cat])
    if orden =='preciodesc':
        cursor.execute("SELECT Nombre_Producto ,Precio_Producto ,Imagen ,Codigo_Producto ,Categoria_Nombre_Categoria  From Producto,Categoria WHERE (Categoria_Padre=%s or Nombre_Categoria=%s) AND Nombre_Categoria=Categoria_Nombre_Categoria ORDER BY(Precio_Producto)",[cat,cat])
    if orden=='precioasc':
         cursor.execute("SELECT Nombre_Producto ,Precio_Producto ,Imagen ,Codigo_Producto ,Categoria_Nombre_Categoria  From Producto,Categoria WHERE (Categoria_Padre=%s or Nombre_Categoria=%s) AND Nombre_Categoria=Categoria_Nombre_Categoria ORDER BY(Precio_Producto) DESC",[cat,cat])
    if orden=='fehaasc':
         cursor.execute("SELECT Nombre_Producto ,Precio_Producto ,Imagen ,Codigo_Producto ,Categoria_Nombre_Categoria  From Producto,Categoria WHERE (Categoria_Padre=%s or Nombre_Categoria=%s) AND Nombre_Categoria=Categoria_Nombre_Categoria ORDER BY(Fecha_Publicacion) DESC",[cat,cat])
    if orden=='fehadesc':
        cursor.execute("SELECT Nombre_Producto ,Precio_Producto ,Imagen ,Codigo_Producto ,Categoria_Nombre_Categoria  From Producto,Categoria WHERE (Categoria_Padre=%s or Nombre_Categoria=%s) AND Nombre_Categoria=Categoria_Nombre_Categoria ORDER BY(Fecha_Publicacion)",[cat,cat])
    rows = cursor.fetchall()
    CategoriaLista=Categoria.objects.filter(categoria_padre=cat)
    if not CategoriaLista:
        categoria_padre=get_object_or_404(Categoria,categoria=cat)
        CategoriaLista=Categoria.objects.filter(categoria_padre=categoria_padre.nombre_categoria)
    Lista=rows
    context_data = {"cat":cat,"form":form,"object_list": Lista,"lista_categoria":CategoriaLista,"titulo":cat}
    return render(request, "producto2.html", context_data)

def Algo(request,cat,pro):
	form=BuscarForm(request.POST or None)
	titulo="Busqueda: \'"+str(pro)+"\'"
	if form.is_valid():
		producto=form.cleaned_data['Producto']
		categoria=form.cleaned_data['Categoria']
		return redirect("/Busqueda/"+str(producto)+"/"+str(categoria))
	CategoriaLista=Categoria.objects.filter(categoria_padre=None)
	consulta="SELECT * FROM PRODUCTO WHERE LOWER(NOMBRE_PRODUCTO) like \'%"+str(pro).lower()+"%\'"
	cursor=connection.cursor()
	if str(cat) == 'None':
		cursor.execute(consulta)
		rows = cursor.fetchall()
	else:
		cursor.execute(consulta+" AND CATEGORIA_NOMBRE_CATEGORIA =\'"+str(cat)+"\'")
		rows = cursor.fetchall()
		titulo+=" en "+str(cat)
	context_data = {"form":form,"object_list": rows,"lista_categoria":CategoriaLista,"titulo":titulo}
	return render(request, "Busquedas.html", context_data)

def Mis_ProductosCategoria(request,cat):
	if not request.user.is_staff or request.user.is_superuser:
		return render(request, "404.html")
	usuario=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	CategoriaLista=Categoria.objects.filter(categoria_padre=cat)
	if not CategoriaLista:
		categoria_padre=get_object_or_404(Categoria,categoria=cat)
		CategoriaLista=Categoria.objects.filter(categoria_padre=categoria_padre.nombre_categoria)
	cursor=connection.cursor()
	cursor.execute("SELECT Nombre_Producto,Codigo_Producto,categoria_nombre_categoria,imagen,precio_producto FROM Categoria, PRODUCTO, DETALLE_PRODUCTO, USUARIO WHERE (PRODUCTO.CATEGORIA_NOMBRE_CATEGORIA=Categoria.NOMBRE_CATEGORIA or PRODUCTO.CATEGORIA_NOMBRE_CATEGORIA=Categoria.CATEGORIA_PADRE) AND PRODUCTO.CODIGO_PRODUCTO=DETALLE_PRODUCTO.CODIGO_PD AND DETALLE_PRODUCTO.PROPIETARIO=USUARIO.ID_USUARIO AND ID_USUARIO=%s AND (Categoria_Padre=%s or CATEGORIA.Nombre_Categoria=%s)",[usuario.id_usuario,cat,cat])
	rows = cursor.fetchall()
	context_data = {"SI":'S',"object_list": rows,"lista_categoria":CategoriaLista,"titulo":"Mis Productos: "+str(cat)}
	return render(request, "misproductos.html", context_data)

def Editar_Producto(request,pro):
	if not request.user.is_staff or request.user.is_superuser:
		return render(request, "404.html")
	producto=get_object_or_404(Producto,codigo_producto=pro)
	detalle=get_object_or_404(DetalleProducto,codigo_pd=producto)
	usuario=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	if detalle.propietario != usuario:
	 	return render(request, "404.html")
	form=ProductoEditarForm(request.POST or None,initial={'Nombre_Producto': producto.nombre_producto,'Categoria': producto.categoria_nombre_categoria,'Precio':producto.precio_producto,'Cantidad':producto.cantidad,'Descripcion':producto.descripcion})
	if form.is_valid():
		producto.nombre_producto=form.cleaned_data['Nombre_Producto']
		producto.cantidad=form.cleaned_data['Cantidad']
		producto.precio_producto=form.cleaned_data['Precio']
		producto.descripcion=form.cleaned_data['Descripcion']
		producto.categoria_nombre_categoria=form.cleaned_data['Categoria']
		producto.save()
		return redirect('/Mis_Productos/')
	context_data = {"form" : form,"Accion": "Guardar","titulo":producto.nombre_producto}
	return render(request, "form.html", context_data)

def Producto_Invidual(request,pro):
	form=ComentForm(request.POST or None)
	form2=ComprarForm(request.POST or None)
	cursor=connection.cursor()
	cursor.execute("SELECT * FROM COMENTARIO_Producto WHERE PRODUCTO=%s",[pro])
	comentarios = cursor.fetchall()
	producto=get_object_or_404(Producto,codigo_producto=pro)
	detalle=get_object_or_404(DetalleProducto,codigo_pd=pro)
	cursor.execute("SELECT Nombre_Cp FROM Color_Producto WHERE Cod_Producto =%s",[pro])
	colores = cursor.fetchall()
	cursor.execute("SELECT SUM(PUNTUACION) ,Count(PUNTUACION),(SUM(PUNTUACION)/Count(PUNTUACION))  FROM COMENTARIO_PRODUCTO WHERE PRODUCTO=%s",[pro])
	punts=0;
	punt = cursor.fetchall()
	for obj in punt:
		punts=obj[2]
	usuario=None
	Pro='N'
	usuario2=get_object_or_404(Usuario,id_usuario=detalle.propietario.id_usuario)
	if request.user.is_staff:
		usuario=get_object_or_404(Usuario, id_usuario=request.user.get_username)
		if(usuario.id_usuario==usuario2.id_usuario):
			Pro='S';
	if form2.is_valid() or form.is_valid():
		if form2.is_valid():
			cantidad=form2.cleaned_data['Cantidad']
			print(cantidad)
			if cantidad > producto.cantidad:
				context_data = {"puntuacion":punts,"prop":Pro,"colores":colores,"comentarioslist":comentarios,"error":'S',"form" : form,"form2" : form2,"product": producto,"us":usuario2,"detalle":detalle,"titulo":producto.nombre_producto}
				return render(request, "producto.html", context_data)
			else:
				hay=0
				diferencia=producto.cantidad-cantidad
				cursor.execute("UPDATE PRODUCTO SET Cantidad=%s WHERE CODIGO_PRODUCTO=%s;",[diferencia,pro])
				cursor.execute("SELECT * FROM Detalle_Carrito WHERE Producto_Carrito_Detalle=%s and Usuario_Carrito_Detalle=%s",[pro,usuario.id_usuario])
				rows = cursor.fetchall()
				print(producto.precio_producto)
				carrito=get_object_or_404(Carrito,usuario_id_usuario=usuario)
				if carrito.total_carrito is None:
					carrito.total_carrito=(producto.precio_producto*cantidad)
				else:
					carrito.total_carrito+=(producto.precio_producto*cantidad)
				for obj in rows:
					hay=obj[2]
				if hay ==0:
					ids=0
					cursor.execute("SELECT secuencia_comentario.nextval FROM dual")
					rows = cursor.fetchall()
					for obj in rows:
						ids=obj[0]
					DetalleCarrito.Ingresar_Carrito(ids,cantidad,carrito,producto,producto.precio_producto)
				else:
					hay=hay+cantidad
					cursor.execute("UPDATE Detalle_Carrito SET Cantidad=%s WHERE Producto_Carrito_Detalle=%s and Usuario_Carrito_Detalle=%s",[hay,pro,usuario.id_usuario])
				carrito.save()
				producto.cantidad-=cantidad
				producto.save()
				context_data = {"puntuacion":punts,"prop":Pro,"colores":colores,"comentarioslist":comentarios,"error":'P',"form" : form,"form2" : form2,"product": producto,"us":usuario2,"detalle":detalle,"titulo":producto.nombre_producto}
				return render(request, "producto.html", context_data)
		if form.is_valid():
			ids=0
			contenido=form.cleaned_data['Comentario']
			Puntuacion=form.cleaned_data['Puntuacion']
			usua=request.user.get_full_name()
			print(usua)
			cursor.execute("SELECT secuencia_comentario.nextval FROM dual")
			rows = cursor.fetchall()
			for obj in rows:
				ids=obj[0]
			ComentarioProducto.Ingresar_Comentario(ids,usua,producto,contenido,Puntuacion)
			return redirect('/Producto/'+str(pro))
	context_data = {"puntuacion":punts,"prop":Pro, "colores":colores,"comentarioslist":comentarios,"form" : form,"form2" : form2,"product": producto,"us":usuario2,"detalle":detalle,"titulo":producto.nombre_producto}
	return render(request, "producto.html", context_data)

def Crear_Producto(request):
	if not request.user.is_staff:
		return render(request, "404.html")
	form=ProductosForm(request.POST or None,request.FILES or None)
	if form.is_valid():
		codigo=form.cleaned_data['Codigo_Producto']
		nombre=form.cleaned_data['Nombre_Producto']
		cantidad=form.cleaned_data['Cantidad']
		precio=form.cleaned_data['Precio']
		descripcion=form.cleaned_data['Descripcion']
		categoria=form.cleaned_data['Categoria']
		nombre_imagen=str(codigo)+str('.png')
		Producto.Ingresar_Producto(codigo,nombre,nombre_imagen,cantidad,descripcion,precio,categoria)
		subir_imagen_producto(request.FILES['Imagen'],nombre_imagen)
		propietario=get_object_or_404(Usuario,id_usuario=request.user.get_username)
		producto=get_object_or_404(Producto,codigo_producto=codigo)
		DetalleProducto.Ingresar_DetalleProducto(propietario,producto)
		return redirect('/Producto/'+str(codigo))
	context_data = {"form" : form,"Accion": "Guardar","titulo":"Nuevo Producto"}
	return render(request, "form.html", context_data)


def subir_imagen_producto(f,nombre):
    with open("/home/alfredo/Escritorio/Fase_3/media_cdn/productos/"+str(nombre), 'w+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def Editar_Usuario2(request,id):
	if not request.user.is_superuser:
		return render(request, "404.html")
	instance=get_object_or_404(Usuario,id_usuario=id)
	form=EditarUForm(request.POST or None, instance=instance)
	if form.is_valid():
	   instance=form.save(commit=False)
	   instance.save()
	   user2=get_object_or_404(AuthUser,username=id)
	   user2.first_name=instance.nombre_usuario
	   user2.last_name=instance.apellido_usuario
	   user2.save()
	   u = User.objects.get(username=id)
	   u.set_password(instance.password_usuario)
	   u.save()
	   cursor=connection.cursor()
	   ids=0
	   cursor.execute("SELECT secuencia_comentario.nextval FROM dual")
	   rows = cursor.fetchall()
	   for obj in rows:
	   	ids=obj[0]
	   users=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	   Bitacora.Ingresar_Bitacora(ids,users,"Editar","Se edito el Usuario: "+str(id))
	   return redirect("/Admin/Usuario/")
	context_data = {"form" : form,"Accion": "Guardar","titulo":"Editar Usuario (Admin)"}
	return render(request, "form.html", context_data)

def Admin_Productos(request):
	if not request.user.is_superuser:
		return redirect ("/404/")
	productos=Producto.objects.all()
	context_data = {"productos": productos,"titulo":"Lista de Productos"}
	return render(request, "listaproductos.html", context_data)

def Eliminar_Producto2(request,pro):
	if  not request.user.is_superuser:
		return render(request, "404.html")
	producto=get_object_or_404(Producto,codigo_producto=pro)
	producto.delete()
	cursor=connection.cursor()
	ids=0
	cursor.execute("SELECT secuencia_comentario.nextval FROM dual")
	rows = cursor.fetchall()
	for obj in rows:
		ids=obj[0]
	users=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	Bitacora.Ingresar_Bitacora(ids,users,"Eliminar","Se elimino el Producto: "+str(pro))
	return redirect('/Admin/Productos/')

def Editar_Producto2(request,pro):
	if not request.user.is_superuser:
		return render(request, "404.html")
	instance=get_object_or_404(Producto,codigo_producto=pro)
	form=EditarProducto2(request.POST or None, instance=instance)
	if form.is_valid():
	   instance=form.save(commit=False)
	   instance.save()
	   cursor=connection.cursor()
	   ids=0
	   cursor.execute("SELECT secuencia_comentario.nextval FROM dual")
	   rows = cursor.fetchall()
	   for obj in rows:
	   	ids=obj[0]
	   users=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	   Bitacora.Ingresar_Bitacora(ids,users,"Editar","Se edito el Producto: "+str(pro))
	   return redirect("/Admin/Productos/")
	context_data = {"form" : form,"Accion": "Guardar","titulo":"Editar Producto (Admin)"}
	return render(request, "form.html", context_data)

def Admin_Categorias(request):
	if not request.user.is_superuser:
		return redirect ("/404/")
	categorias=Categoria.objects.all()
	context_data = {"categorias": categorias,"titulo":"Lista de Categorias"}
	return render(request, "listacategorias.html", context_data)

def Editar_Categoria(request,cat):
	if not request.user.is_superuser:
		return render(request, "404.html")
	categoria=get_object_or_404(Categoria,nombre_categoria=cat)
	form=CategoriaForm(request.POST or None,instance=categoria)
	if form.is_valid():
		instance=form.save(commit=False)
		instance.save()
		cursor=connection.cursor()
		ids=0
		cursor.execute("SELECT secuencia_comentario.nextval FROM dual")
		rows = cursor.fetchall()
		for obj in rows:
			ids=obj[0]
		users=get_object_or_404(Usuario,id_usuario=request.user.get_username)
		Bitacora.Ingresar_Bitacora(ids,users,"Editar","Se edito La Categoria: "+str(cat))
		return redirect("/Admin/Categorias/")
	context_data = {"form" : form,"Accion": "Guardar","titulo":"Nueva Categoria"}
	return render(request, "form.html", context_data)

def Eliminar_Categoria(request,cat):
	if not request.user.is_superuser:
		return render(request, "404.html")
	categoria=get_object_or_404(Categoria,nombre_categoria=cat)
	categoria.delete()
	cursor=connection.cursor()
	ids=0
	cursor.execute("SELECT secuencia_comentario.nextval FROM dual")
	rows = cursor.fetchall()
	for obj in rows:
		ids=obj[0]
	users=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	Bitacora.Ingresar_Bitacora(ids,users,"Eliminar","Se elimino la Categoria: "+str(cat))
	return redirect("/Admin/Categorias/")

def Carrito_Usuario(request,id):
	if not request.user.is_superuser:
		return render(request, "404.html")
	usuario=get_object_or_404(Usuario,id_usuario=id)
	form=CompraCarritoForm(request.POST or None)
	carrito=get_object_or_404(Carrito,usuario_id_usuario=id)
	cursor=connection.cursor()
	cursor.execute("Select Codigo_Producto, Nombre_Producto, DETALLE_CARRITO.CANTIDAD,Precio_Producto, (Precio_Producto*DETALLE_CARRITO.CANTIDAD) as Sub_Total FROM PRODUCTO, DETALLE_CARRITO, CARRITO, USUARIO WHERE ID_USUARIO=%s AND USUARIO.ID_USUARIO = CARRITO.USUARIO_ID_USUARIO AND DETALLE_CARRITO.USUARIO_CARRITO_DETALLE=USUARIO.ID_USUARIO AND PRODUCTO.CODIGO_PRODUCTO= DETALLE_CARRITO.PRODUCTO_CARRITO_DETALLE;",[id])
	rows = cursor.fetchall()
	context_data = {"id":id,"productos": rows,"titulo":"Carrito: "+str(usuario.nombre_usuario)}
	return render(request, "carrito2.html", context_data)

def EliminarProducto_Carrito2(request,id,pro):
	if not request.user.is_superuser:
		return render(request, "404.html")
	ids=id
	producto=get_object_or_404(Producto,codigo_producto=pro)
	carrito=get_object_or_404(Carrito,usuario_id_usuario=ids)
	detalle=get_object_or_404(DetalleCarrito, usuario_carrito_detalle=ids, producto_carrito_detalle=pro)
	producto.cantidad+=detalle.cantidad
	carrito.total_carrito-=(producto.precio_producto*detalle.cantidad)
	producto.save()
	carrito.save()
	detalle.delete()
	return redirect('/Editar/Carrito/'+str(id))

def carga_masiva(f,id):
	cursor=connection.cursor()
	for linea in f.chunks():
		filas=str(linea).replace(", ",",").split('\n')
		for fila in filas:
			producto=fila.split(",")
			categorias=str(producto[4]).split("-")
			cursor.execute("SELECT * FROM Categoria WHERE Nombre_Categoria =%s",[categorias[0]])
			rows = cursor.fetchall()
			if not rows:
				cat=Categoria()
				cat.nombre_categoria=categorias[0]
				cat.save()
			cursor.execute("SELECT * FROM Categoria WHERE Nombre_Categoria =%s",[categorias[1]])
			rows = cursor.fetchall()
			if not rows:
				cat=Categoria()
				cat.nombre_categoria=categorias[1]
				cat.categoria_padre=get_object_or_404(Categoria,nombre_categoria=categorias[0])
				cat.save()
			pro=Producto()
			pro.codigo_producto=producto[0]
			pro.nombre_producto=producto[1]
			pro.descripcion=producto[3]
			pro.categoria_nombre_categoria=get_object_or_404(Categoria,nombre_categoria=categorias[1])
			pro.imagen=str(producto[0])+".png"
			pro.precio_producto=producto[5]
			pro.cantidad=producto[6]
			pro.fecha_publicacion=str("20")+time.strftime("%y-%m-%d")
			pro.save()
			print(pro.categoria_nombre_categoria)
			shutil.copyfile(str(producto[2]),"/home/alfredo/Escritorio/Fase_3/media_cdn/productos/"+str(pro.imagen))
			propietario=get_object_or_404(Usuario,id_usuario=id)
			DetalleProducto.Ingresar_DetalleProducto(propietario,pro)
			colores=str(producto[7]).split('-')
			for color in colores:
				cursor.execute("SELECT * FROM Color WHERE Nombre_Color =%s",[color])
				rows = cursor.fetchall()
				if not rows:
					col=Color()
					col.nombre_color=color
					col.save()
				cp=ColorProducto()
				cp.cod_producto=pro
				cp.nombre_cp=get_object_or_404(Color,nombre_color=color)
				cursor.execute("SELECT secuencia_comentario.nextval FROM dual")
				rows = cursor.fetchall()
				for obj in rows:
					cp.id_colorproducto=obj[0]
				cp.save()

def Carga_Masiva(request):
	if not request.user.is_staff:
		return render(request, "404.html")
	form=CargaMasivaForm(request.POST or None,request.FILES or None)
	if form.is_valid():
		carga_masiva(form.cleaned_data['Arhivo_CSV'],request.user.get_username)
	context_data = {"form" : form,"Accion": "Cargar","titulo":"Carga Masiva"}
	return render(request, "form.html", context_data)

def Eliminar_Comentario(request,pro,ids):
	if request.user.is_staff:
		get_object_or_404(ComentarioProducto,id_comentaroi=ids).delete()
		return redirect("/Producto/"+pro);
	else:
		return render(request, "404.html")

def Bitacoras(request):
	if not request.user.is_superuser:
		return render(request, "404.html")
	usuario=get_object_or_404(Usuario,id_usuario=request.user.get_username)
	bitacoras=Bitacora.objects.filter(usuario_bitacora=usuario)
	context_data = {"bitacoras": bitacoras,"titulo":"Bitacora: "+usuario.nombre_usuario}
	return render(request, "bitacoras.html", context_data)

def Reporte_3(request):
	form=Reporte3Form(request.POST or None)
	if form.is_valid():
		fecha=form.cleaned_data['Fecha']
		return Rep3(request,fecha)
	context_data = {"form" : form,"Accion": "Aceptar","titulo":"Reporte de Administadoras"}
	return render(request, "form.html", context_data)

def Rep3(request,fecha):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Listado de Administradores que naciern antes de "+str(fecha), styles['Heading1'])
	clientes.append(header)
	headings = ('Nombre', 'Apellido', 'Clase', 'Genero','Fecha Nacimiento')
	cursor.execute("SELECT Nombre_USUARIO,Apellido_Usuario,Clase_Cliente,Genero,Fecha_Nacimiento FROM USUARIO WHERE Clase_Cliente='S' AND (Genero='F' or Genero='f') AND Fecha_Nacimiento<%s",[fecha])
	allclientes = [(p[0], p[1], p[2], p[3],p[4]) for p in cursor.fetchall()]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (5, -1), 1, colors.dodgerblue),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

def Rep4(request):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Listado de Clientes ordenados por Ganancia", styles['Heading1'])
	clientes.append(header)
	headings = ('ID','Nombre', 'Apellido', 'Ganancia')
	cursor.execute("SELECT ID_Usuario,Nombre_Usuario,Apellido_Usuario,Ganancia_Usuario FROM USUARIO ORder By(Ganancia_USUARIO) DESC;")
	allclientes = [(p[0], p[1], p[2], p[3]) for p in cursor.fetchall()]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

def Rep6(request):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Listado de Productos mas vendidos", styles['Heading1'])
	clientes.append(header)
	headings = ('Codigo Producto','Nombre Producto','Total Ganancias', '# Articulos Vendido')
	cursor.execute("SELECT PRODUCTO.CODIGO_PRODUCTO,SUM(PRECIO_UNIDAD),SUM(Cantidad_Factura) FROM PRODUCTO,DETALLE_FACTURA WHERE Producto_Codigo_Producto=Codigo_Producto GROUP BY(DETALLE_FACTURA.PRODUCTO_CODIGO_PRODUCTO) ORDER BY(SUM(Cantidad_Factura)) DESC")
	allclientes = [(p[0],get_object_or_404(Producto,codigo_producto=p[0]).nombre_producto, p[1], p[2]) for p in cursor.fetchall()]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

def Rep7(request):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Listado de Usuarios con mas articulos publicados", styles['Heading1'])
	clientes.append(header)
	headings = ('ID','Usuario',"Apellido","Ganancias", '# Articulos')
	cursor.execute("SELECT Propietario,COUNT(Propietario) FROM DETALLE_PRODUCTO  GROUP BY (Propietario) ORDER BY(COUNT(Propietario))DESC;")
	allclientes = [(p[0],get_object_or_404(Usuario,id_usuario=p[0]).nombre_usuario,get_object_or_404(Usuario,id_usuario=p[0]).apellido_usuario,get_object_or_404(Usuario,id_usuario=p[0]).ganancia_usuario, p[1]) for p in cursor.fetchall()]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (4, -1), 1, colors.dodgerblue),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

def Rep8(request):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Listado de Productos y sus Categorias", styles['Heading1'])
	clientes.append(header)
	headings = ('Codigo','Nombre',"Precio","Categoria", 'Categoria Padre')
	cursor.execute("SELECT Codigo_Producto,Nombre_Producto,Precio_Producto,CATEGORIA_NOMBRE_CATEGORIA FROM PRODUCTO;")
	allclientes = [(p[0],p[1],p[2],p[3],get_object_or_404(Categoria,nombre_categoria=p[3]).categoria_padre) for p in cursor.fetchall()]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (4, -1), 1, colors.dodgerblue),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

def Reporte_9(request):
	form=Reporte9Form(request.POST or None)
	if form.is_valid():
		fecha=form.cleaned_data['Fecha']
		return Rep9(request,fecha)
	context_data = {"form" : form,"Accion": "Aceptar","titulo":"Reporte de Comentarios en Y Fecha"}
	return render(request, "form.html", context_data)

def Rep9(request,fecha):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Total de Comentarios en "+str(fecha), styles['Heading1'])
	clientes.append(header)
	headings = ('Codigo', 'Nombre Producto', '# Comentario')
	cursor.execute("SELECT PRODUCTO,Count(PRODUCTO) FROM COMENTARIO_PRODUCTO WHERE Fecha=%s Group By(PRODUCTO)",[fecha])
	allclientes = [(p[0],get_object_or_404(Producto,codigo_producto=p[0]).nombre_producto,p[1]) for p in cursor.fetchall()]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (2, -1), 1, colors.dodgerblue),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

def Reporte_10(request):
	form=Reporte10Form(request.POST or None)
	if form.is_valid():
		fecha=form.cleaned_data['Cantidad']
		return Rep10(request,fecha)
	context_data = {"form" : form,"Accion": "Aceptar","titulo":"Reporte de Productos con N Cantidad"}
	return render(request, "form.html", context_data)

def Rep10(request,fecha):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Producto con "+str(fecha)+" Unidades", styles['Heading1'])
	clientes.append(header)
	headings = ('Codigo', 'Nombre Producto','Categoria', 'Cantidad','Precio')
	allclientes = [(p.codigo_producto,p.nombre_producto,p.categoria_nombre_categoria,p.cantidad,p.precio_producto) for p in Producto.objects.filter(cantidad=fecha)]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (4, -1), 1, colors.dodgerblue),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

def Rep5(request):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Puntuacions de Productos", styles['Heading1'])
	clientes.append(header)
	headings = ('Codigo', 'Nombre Producto', '# Puntuacions','Suma de Puntuacions','Promedio')
	cursor.execute("SELECT PRODUCTO,SUM(PUNTUACION) ,Count(PUNTUACION),(SUM(PUNTUACION)/Count(PUNTUACION))  FROM COMENTARIO_PRODUCTO GROUP BY(PRODUCTO) ORDER BY((SUM(PUNTUACION)/Count(PUNTUACION))) DESC")
	allclientes = [(p[0],get_object_or_404(Producto,codigo_producto=p[0]).nombre_producto,p[2],p[1],p[3]) for p in cursor.fetchall()]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (4, -1), 1, colors.dodgerblue),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

def Reporte_2(request):
	form=Reporte3Form(request.POST or None)
	if form.is_valid():
		fecha=form.cleaned_data['Fecha']
		return Rep2(request,fecha)
	context_data = {"form" : form,"Accion": "Aceptar","titulo":"Help Desk nacidos despues de Y fecha"}
	return render(request, "form.html", context_data)

def Editar_Datos(request):
    if not request.user.is_superuser:
        return render(request, "404.html")
    datos=get_object_or_404(Datos,id_datos=0)
    form=DatosForm(request.POST or None,initial={"Mision":datos.mision,"Vision":datos.vision,"Acerca_De":datos.acerca_de})
    if form.is_valid():
        mision=form.cleaned_data['Mision']
        vision=form.cleaned_data['Vision']
        acerca=form.cleaned_data['Acerca_De']
        datos.mision=mision
        datos.vision=vision
        datos.acerca_de=acerca
        datos.save()
        return redirect("/Inicio/")
    context_data = {"form" : form,"Accion": "Aceptar","titulo":"Editar Datos de la Pagina Web"}
    return render(request, "form.html", context_data)

def Inicio(request):
    form=Reporte3Form(request.POST or None)
    datos=get_object_or_404(Datos,id_datos=0)
    context_data = {"Mision" : datos.mision,"Vision" : datos.vision,"Acerca" : datos.acerca_de,"Accion": "Aceptar","titulo":"Help Desk nacidos despues de Y fecha"}
    return render(request, "Inicios.html",context_data)

def Rep2(request,fecha):
	cursor=connection.cursor()
	print "Genero el PDF"
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "clientes.pdf"  # llamado clientes
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
	clientes = []
	styles = getSampleStyleSheet()
	header = Paragraph("Listado de Help Desk que naciern despues de "+str(fecha), styles['Heading1'])
	clientes.append(header)
	headings = ('Nombre', 'Apellido', 'Clase', 'Genero','Fecha Nacimiento')
	cursor.execute("SELECT Nombre_USUARIO,Apellido_Usuario,Clase_Cliente,Genero,Fecha_Nacimiento FROM USUARIO WHERE Clase_Cliente='H' AND (Genero='M' or Genero='m') AND Fecha_Nacimiento>%s",[fecha])
	allclientes = [(p[0], p[1], p[2], p[3],p[4]) for p in cursor.fetchall()]
	print allclientes
	t = Table([headings] + allclientes)
	t.setStyle(TableStyle(
		[
		('GRID', (0, 0), (5, -1), 1, colors.HexColor(0x354D62)),
		('LINEBELOW', (0, 0), (-1, 0), 2, colors.HexColor(0xD7F2F7)),
		('BACKGROUND', (0, 0), (-1, 0), colors.HexColor(0x718CA1))
		]
		))
	clientes.append(t)
	doc.build(clientes)
	response.write(buff.getvalue())
	buff.close()
	return response

