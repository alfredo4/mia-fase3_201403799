package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Admin extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Usuario user = new Usuario();
    ArrayList<String> Usuarios =new ArrayList <String>();
    ArrayList<String> UserArray =new ArrayList <String>();
    TextView nombre,correo, credito,gancias,clase;
    String mision,vision,acerca;
    ImageView foto;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO";
    String password="password";
    String puerto="1521";
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try{
                UserArray = bundle.getStringArrayList("User");
                user.FromArrayString(UserArray);
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }
        try {
            nombre=(TextView) findViewById(R.id.nombre_cliente2);
            correo=(TextView) findViewById(R.id.Correo_Cliente2);
            credito=(TextView) findViewById(R.id.credito_cliente2);
            gancias=(TextView) findViewById(R.id.ganancias_cliente2);
            clase =(TextView) findViewById(R.id.clase_cliente3);
            nombre.setText(user.getNombre()+" "+user.getApellido());
            correo.setText(user.getCorreo());
            credito.setText("Telefono: "+user.getTelefono());
            gancias.setText("Direccion: "+user.getDireccion());
            clase.setText(clase.getText()+" "+user.getClase());
            foto=(ImageView) findViewById(R.id.Foto22) ;
            Picasso.with(this).load("http://192.168.0.8:8000/media/"+user.getId()+".png").resize(200,200).error(R.drawable.logo3).resize(200,200).into(foto);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent form = new Intent(Admin.this, Perfil.class);
            form.putExtra("User",user.toArrayString());
            form.putExtra("Editar",user.toArrayString());
            startActivity(form);
        } else if (id == R.id.nav_gallery) {
            Intent form = new Intent(Admin.this, Registro.class);
            form.putExtra("Admin","YES");
            form.putExtra("User",user.toArrayString());
            startActivity(form);

        }
        else if (id == R.id.datos) {
            ObtenerDatos();
            Intent form = new Intent(Admin.this, Datos.class);
            form.putExtra("User",user.toArrayString());
            form.putExtra("Mision",mision);
            form.putExtra("Vision",vision);
            form.putExtra("Acerca",acerca);
            startActivity(form);

        }
        else if (id == R.id.nav_slideshow) {
            ObtenerUsuarios();
            Intent form = new Intent(Admin.this, Usuarios.class);
            form.putExtra("Usuarios",Usuarios);
            form.putExtra("User",user.toArrayString());
            startActivity(form);

        }  else if (id == R.id.nav_send) {
            Intent form = new Intent(Admin.this, Inicio.class);
            startActivity(form);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void ObtenerUsuarios()
    {
        Usuarios.clear();
        try{
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery("SELECT * FROM USUARIO");
            while(resultado.next())
            {
                Usuarios.add(resultado.getString("ID_Usuario")+"\n"+resultado.getString("Nombre_Usuario")+"\nClase: "+resultado.getString("Clase_Cliente"));
            }
            resultado.close();
            st.close();
            con.close();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }
    }
    private void ObtenerDatos()
    {
        try{
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery("SELECT * FROM Datos");
            while(resultado.next())
            {
               mision= resultado.getString("Mision");
                vision= resultado.getString("VISION");
                acerca= resultado.getString("ACERCA_DE");
            }
            resultado.close();
            st.close();
            con.close();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }
    }
}
