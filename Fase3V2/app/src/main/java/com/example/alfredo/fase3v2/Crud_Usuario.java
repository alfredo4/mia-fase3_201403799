package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by alfredo on 8/11/16.
 */

public class Crud_Usuario extends AppCompatActivity {
    Usuario user = new Usuario();
    Usuario busqueda = new Usuario();
    ArrayList<String> Productos =new ArrayList <String>();
    ArrayList<String> UserArray =new ArrayList <String>();
    TextView nombre,correo, credito,pass,clase,telefono;
    ImageView foto;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO";
    String password="password";
    String puerto="1521";
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crud_usuario);
        Bundle bundle = getIntent().getExtras();
        nombre=(TextView) findViewById(R.id.nombre_cliente3);
        correo=(TextView) findViewById(R.id.Correo_Cliente3);
        credito=(TextView) findViewById(R.id.credito_cliente3);
        pass=(TextView) findViewById(R.id.Password_Cliente);
        clase=(TextView) findViewById(R.id.clase_cliente3);
        telefono=(TextView) findViewById(R.id.Telefono_Cliente3);
        foto=(ImageView) findViewById(R.id.FOTO8);
        if (bundle != null) {
            try{
                UserArray = bundle.getStringArrayList("User");
                user.FromArrayString(UserArray);
                UserArray = bundle.getStringArrayList("Busqueda");
                busqueda.FromArrayString(UserArray);
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }
        nombre.setText(busqueda.getNombre()+" "+busqueda.getApellido());
        correo.setText(busqueda.getCorreo());
        credito.setText("Credito: Q."+busqueda.getCredito());
        pass.setText("Password: "+busqueda.getPass());
        clase.setText("Clase de usuario: "+busqueda.getClase());
        telefono.setText("Telefono: "+busqueda.getTelefono());
        Picasso.with(this).load("http://192.168.0.8:8000/media/"+busqueda.getId()+".png").resize(200,200).error(R.drawable.logo3).resize(200,200).into(foto);

    }
    public void Boton_Eliminar(View v) {
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            String consulta="BEGIN \n";
            consulta+="eliminar_usuarioandroid("+busqueda.getId()+");\n";
            consulta+="\n END;";
            st.executeQuery(consulta);
            st.close();
            con.close();
            Toast.makeText(getApplicationContext(),"Usuario Eliminado",Toast.LENGTH_LONG).show();
            Intent form = new Intent(Crud_Usuario.this, Admin.class);
            form.putExtra("User",user.toArrayString());
            startActivity(form);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }

    }
    public  void  Boton_Editar(View v)
    {
        Intent form = new Intent(Crud_Usuario.this, Perfil.class);
        form.putExtra("User",user.toArrayString());
        form.putExtra("Editar",busqueda.toArrayString());
        startActivity(form);
    }
}
