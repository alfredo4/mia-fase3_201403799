package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by alfredo on 8/11/16.
 */

public class Producto extends AppCompatActivity {
    ImageView imagem;
    Usuario user= new Usuario();
    ArrayList<String> UserArray =new ArrayList <String>();
    TextView Nombre,Codigo,Categoria,Precio,Fecha,Cantidad,Descripcion,label;
    EditText cantidad;
    Button Compra;
    String Foto,fech;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO";
    String password="password";
    String puerto="1521";
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    String cod,precio,cants;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pro_individual);
        
        Nombre = (TextView) findViewById(R.id.nombre);
        Codigo = (TextView) findViewById(R.id.codigo);
        Categoria = (TextView) findViewById(R.id.propietario);
        Precio = (TextView) findViewById(R.id.precio);
        Fecha = (TextView) findViewById(R.id.fecha);
        Cantidad = (TextView) findViewById(R.id.cant);
        Descripcion = (TextView) findViewById(R.id.descrip);
        cantidad=(EditText) findViewById(R.id.cant_compra);
        Compra=(Button) findViewById(R.id.boton_comp) ;
        label=(TextView) findViewById(R.id.cant_label) ;
       Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try{

                if(bundle.getStringArrayList("User")==null)
                {}
                else {
                    UserArray = bundle.getStringArrayList("User");
                user.FromArrayString(UserArray);}
                cod=bundle.getString("Codigo");
                precio=bundle.getString("Precio");
                cants=bundle.getString("Cantidad");
                Nombre.setText("Nombre: "+bundle.getString("Nombre"));
                Codigo.setText("Codigo: "+bundle.getString("Codigo"));
                Categoria.setText("Categoria: "+bundle.getString("Categoria"));
                Precio.setText("Precio: Q."+bundle.getString("Precio"));
                fech=bundle.getString("Fecha");
                if(fech.length()>12)
                {
                    fech=fech.substring(0,11);
                }
                Cantidad.setText("Cantidad: "+bundle.getString("Cantidad"));
                Descripcion.setText("Descripcion: "+bundle.getString("Descripcion"));
                Foto=bundle.getString("Codigo")+".png";
                Fecha.setText("Fecha de publicacion: "+fech);
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }
        if(user.getNombre().equals(""))
        {
            label.setVisibility(View.INVISIBLE);
            Compra.setVisibility(View.INVISIBLE);
            cantidad.setVisibility(View.INVISIBLE);
        }
        imagem = (ImageView) findViewById(R.id.imageView11);
        Picasso.with(this).load("http://192.168.0.8:8000/media/productos/"+Foto).resize(200,200)
                .into(imagem);
    }
   public void Boton_Comprar(View v)
    {
        int cant= Integer.parseInt(cants);
        int cant2=Integer.parseInt(cantidad.getText().toString());

        if(cant<cant2)
        {
            Toast.makeText(getApplicationContext(),"No temos tantas unidades!",Toast.LENGTH_LONG).show();
        }
        else
        {
            int resta=cant-cant2;
            Agregar_Carrito(cant2,resta);
            cants=String.valueOf(resta);
            Cantidad.setText("Cantidad: "+cants);
        }
    }
    public void Agregar_Carrito(int cantidad2,int dif) {
        try {
            String id,cantidad;
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery("SELECT * FROM Detalle_Carrito WHERE USUARIO_Carrito_Detalle="+user.getId()+" AND Producto_Carrito_Detalle='"+cod+"'");
            if(resultado.next())
            {
                id=resultado.getString("ID_CarritoDetalle");
               cantidad= resultado.getString("Cantidad");
                int total=cantidad2+Integer.parseInt(cantidad);
                st.execute("UPDATE Detalle_Carrito SET Cantidad="+String.valueOf(total)+" WHERE ID_CarritoDetalle="+id);
            }
            else
            {
                id=getID();
               String consulta="BEGIN \n";
                consulta+="carrito_android("+id+","+user.getId()+", '"+cod+"',"+String.valueOf(cantidad2)+", "+precio+");";
                consulta+="\n END;";
                st.execute(consulta);
            }
            st.execute("UPDATE Producto SET Cantidad="+String.valueOf(dif)+" WHERE Codigo_Producto='"+cod+"'");
            resultado.close();
            st.close();
            con.close();
            Toast.makeText(getApplicationContext(),"Se ha agregado el producto a su carrito!",Toast.LENGTH_LONG).show();

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }

    }
    private String getID()
    {
        String id="";
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery("SELECT secuencia_comentario.nextval FROM dual");
            if(resultado.next())
            {
                id=resultado.getString("Nextval");
            }
            resultado.close();
            st.close();
            con.close();

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }

        return id;
    }
}
