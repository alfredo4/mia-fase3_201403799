package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Cliente extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
Usuario user = new Usuario();
    ArrayList<String> Productos =new ArrayList <String>();
    ArrayList<String> UserArray =new ArrayList <String>();
    ArrayList<String> Carrito =new ArrayList <String>();
    ArrayList<String> Carrito2 =new ArrayList <String>();
    TextView nombre,correo, credito,gancias,clase;
    ImageView foto;
    String total="";
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO";
    String password="password";
    String puerto="1521";
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try{
                UserArray = bundle.getStringArrayList("User");
                user.FromArrayString(UserArray);
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }
        try {
            nombre=(TextView) findViewById(R.id.nombre_cliente3);
            correo=(TextView) findViewById(R.id.Correo_Cliente3);
            credito=(TextView) findViewById(R.id.Password_Cliente);
            gancias=(TextView) findViewById(R.id.credito_cliente3);
            clase =(TextView) findViewById(R.id.clase_cliente3);
            nombre.setText(user.getNombre()+" "+user.getApellido());
            correo.setText(user.getCorreo());
            credito.setText(credito.getText()+" Q."+user.getCredito());
            gancias.setText(gancias.getText()+" Q."+user.getGanancia());
            clase.setText(clase.getText()+" "+user.getClase());
            foto=(ImageView) findViewById(R.id.FOTO8) ;
            Picasso.with(this).load("http://192.168.0.8:8000/media/"+user.getId()+".png").resize(200,200).error(R.drawable.logo3).resize(200,200).into(foto);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cliente, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent form = new Intent(Cliente.this, Perfil.class);
            form.putExtra("User",user.toArrayString());
            form.putExtra("Editar",user.toArrayString());
            startActivity(form);
        } else if (id == R.id.nav_gallery) {
            ObtenerProductos();
            Intent form = new Intent(Cliente.this, Productos.class);
            form.putExtra("User",user.toArrayString());
            form.putExtra("Productos",Productos);
            startActivity(form);

        } else if (id == R.id.nav_slideshow) {
            ObtenerCarrito();
            Intent form = new Intent(Cliente.this, Carrito.class);
            form.putExtra("User",user.toArrayString());
            form.putExtra("Carrito",Carrito);
            form.putExtra("Carrito2",Carrito2);
            form.putExtra("Total",total);
            startActivity(form);

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {
            Intent form = new Intent(Cliente.this, Inicio.class);
            startActivity(form);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void ObtenerProductos()
    {
        Productos.clear();
        try{
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery("Select * FROM Producto");
            while(resultado.next())
            {
                Productos.add(resultado.getString("Codigo_Producto")+"\n"+resultado.getString("Nombre_Producto")+"\nQ."+resultado.getString("Precio_Producto"));
            }
            resultado.close();
            st.close();
            con.close();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }
    }
    private void ObtenerCarrito()
    {
        Carrito.clear();
        Carrito2.clear();
        double suma=0;
        try{
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery(getCarrito());
            while(resultado.next())
            {
                Carrito.add(resultado.getString("Codigo_Producto")+"\nNombre: "+resultado.getString("Nombre_Producto")+"\nCantidad: "+resultado.getString("Cantidad")+"\nPrecio Q."+resultado.getString("Precio_Producto")+"\nSub_Total Q."+resultado.getString("Sub_total"));
                Carrito2.add(resultado.getString("Codigo_Producto")+"-"+resultado.getString("Nombre_Producto")+"-"+resultado.getString("Cantidad")+"-"+resultado.getString("Precio_Producto")+"-"+resultado.getString("Sub_total"));
                suma+=Double.parseDouble(resultado.getString("Sub_total"));
            }
            resultado.close();
            st.close();
            con.close();
            total=String.valueOf(suma);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }
    }
    private String getCarrito()
    {
        return "Select Codigo_Producto, Nombre_Producto, DETALLE_CARRITO.CANTIDAD,Precio_Producto, (Precio_Producto*DETALLE_CARRITO.CANTIDAD) as Sub_Total FROM PRODUCTO, DETALLE_CARRITO, CARRITO, USUARIO WHERE ID_USUARIO="+user.getId()+" AND USUARIO.ID_USUARIO = CARRITO.USUARIO_ID_USUARIO AND DETALLE_CARRITO.USUARIO_CARRITO_DETALLE=USUARIO.ID_USUARIO AND PRODUCTO.CODIGO_PRODUCTO= DETALLE_CARRITO.PRODUCTO_CARRITO_DETALLE";
    }
}
