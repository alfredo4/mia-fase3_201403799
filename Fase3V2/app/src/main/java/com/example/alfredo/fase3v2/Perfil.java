package com.example.alfredo.fase3v2;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by alfredo on 7/11/16.
 */

public class Perfil extends AppCompatActivity {
    Usuario user = new Usuario();
    Usuario editar = new Usuario();
    ArrayList<String> UserEditar =new ArrayList <String>();
    ArrayList<String> UserArray =new ArrayList <String>();
    EditText pass;
    EditText nombre;
    EditText correo;
    EditText dir;
    EditText apellido;
    EditText telefono;
    String id;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO"; //Nombre de usuario de la bd
    String password="password";//Password de la bd
    String puerto="1521"; //puerto
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil);
        nombre=(EditText) findViewById(R.id.nombre_usuario);
        pass=(EditText) findViewById(R.id.password_usuario);
        correo=(EditText) findViewById(R.id.correo_usuario);
        dir=(EditText) findViewById(R.id.dirrecion_usuario) ;
        apellido=(EditText) findViewById(R.id.apellido_usuario);
        telefono=(EditText) findViewById(R.id.telefono_usuario);
         bundle = getIntent().getExtras();
        if (bundle != null) {
            try{
                UserArray = bundle.getStringArrayList("User");
                user.FromArrayString(UserArray);
                UserArray = bundle.getStringArrayList("Editar");
                editar.FromArrayString(UserArray);
           }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }
        nombre.setText(editar.getNombre());
        apellido.setText(editar.getApellido());
        correo.setText(editar.getCorreo());
        dir.setText(editar.getDireccion());
        telefono.setText(editar.getTelefono());
        pass.setText(editar.getPass());
        id=editar.getId();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void BotonGuardar(View v) {

        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st = con.createStatement();
            st.executeQuery("UPDATE USUARIO SET Nombre_Usuario='" + nombre.getText() + "', Apellido_Usuario='" + apellido.getText() + "', Correo='" + correo.getText() + "', Password_Usuario='" + pass.getText() + "',Direccion_Usuario='" + dir.getText() + "',Telefono_Usuario=" + telefono.getText() + " WHERE ID_Usuario=" + id);
            st.close();
            con.close();
            Toast.makeText(getApplicationContext(),"Datos Actualizados: "+id,Toast.LENGTH_LONG).show();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();

        }
    }
    private  void CargarDatos()
    {

    }

}