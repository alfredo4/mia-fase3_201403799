package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by alfredo on 9/11/16.
 */

public class Datos extends AppCompatActivity {
    Usuario user = new Usuario();
    Usuario editar = new Usuario();
    ArrayList<String> UserEditar =new ArrayList <String>();
    ArrayList<String> UserArray =new ArrayList <String>();
    EditText mision,vision,acerca;
    String id;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO"; //Nombre de usuario de la bd
    String password="password";//Password de la bd
    String puerto="1521"; //puerto
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datos_pagina);
        mision = (EditText) findViewById(R.id.mision);
        vision = (EditText) findViewById(R.id.vision);
        acerca = (EditText) findViewById(R.id.acerca);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            try{
                UserArray = bundle.getStringArrayList("User");
                user.FromArrayString(UserArray);
                mision.setText(bundle.getString("Mision"));
                vision.setText(bundle.getString("Vision"));
                acerca.setText(bundle.getString("Acerca"));
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }
    }
    public void BotonGuardarDatos(View v) {

        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st = con.createStatement();
            st.executeQuery("UPDATE Datos SET Mision='" + mision.getText() + "', VISION='" + vision.getText() + "', ACERCA_DE='" + acerca.getText() +"' WHERE ID_DATOS=0");
            st.close();
            con.close();
            Toast.makeText(getApplicationContext(),"Datos Actualizados",Toast.LENGTH_LONG).show();
            Intent form = new Intent(Datos.this, Cliente.class);
            form.putExtra("User",user.toArrayString());
            startActivity(form);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();

        }
    }
}
