package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by alfredo on 8/11/16.
 */

public class Producto_Carrito extends AppCompatActivity {
    TextView nombre,codigo,cantidad,precio,subtotal;
    String informa;
    ArrayList<String> UserArray =new ArrayList <String>();
    Usuario user = new Usuario();
    ImageView foto;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO";
    String password="password";
    String puerto="1521";
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    String cant,cod;
    Connection con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.producto_carrito);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try{
                UserArray = bundle.getStringArrayList("User");
                informa = bundle.getString("Producto");
                user.FromArrayString(UserArray);
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }
        try {
            String[] auxiliar = informa.split("-");
            nombre=(TextView) findViewById(R.id.nombre_carrito);
            codigo=(TextView) findViewById(R.id.codigo_carrito);
            cantidad=(TextView) findViewById(R.id.cantidad_carrito);
            precio=(TextView) findViewById(R.id.precio_carrito);
            subtotal =(TextView) findViewById(R.id.sub_carrito);
            nombre.setText("Nombre: "+auxiliar[1]);
            codigo.setText("Codigo: "+auxiliar[0]);
            cod=auxiliar[0];
            cant=auxiliar[2];
            cantidad.setText("Cantidad: "+auxiliar[2]);
            precio.setText("Precio: "+auxiliar[3]);
            subtotal.setText("Subtotol: "+auxiliar[4]);
            foto=(ImageView) findViewById(R.id.FOTO8) ;
            Picasso.with(this).load("http://192.168.0.8:8000/media/productos/"+auxiliar[0]+".png").resize(200,200).error(R.drawable.logo3).resize(200,200).into(foto);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }
    }
    public void Eliminar_Carrit(View v) {
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            st.executeQuery("DELETE FROM Detalle_Carrito WHERE USUARIO_Carrito_Detalle="+user.getId()+" AND Producto_Carrito_Detalle='"+cod+"'");
            st.execute("UPDATE Producto SET Cantidad="+Obtener_Cantidad()+" WHERE Codigo_Producto='"+cod+"'");
            st.close();
            con.close();
            Toast.makeText(getApplicationContext(),"Se ha eliminado el producto de su carrito!",Toast.LENGTH_LONG).show();
            Intent form = new Intent(Producto_Carrito.this, Cliente.class);
            form.putExtra("User",user.toArrayString());
            startActivity(form);

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }
    }
    private String Obtener_Cantidad()
    {
        String cuanto="0";
        int total=0;
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st = con.createStatement();
            ResultSet resultado=st.executeQuery("Select * FROM Producto WHERE Codigo_Producto='"+cod+"'");
            while(resultado.next())
            {
                cuanto=resultado.getString("Cantidad");
            }
            resultado.close();
            st.close();
            con.close();
            total=Integer.parseInt(cuanto)+Integer.parseInt(cant);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();

        }
        return String.valueOf(total);
    }

}
