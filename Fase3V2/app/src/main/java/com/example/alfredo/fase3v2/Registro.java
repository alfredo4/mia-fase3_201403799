package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by alfredo on 8/11/16.
 */

public class Registro
        extends AppCompatActivity {
    Usuario logueado = new Usuario();
    ArrayList<String> UserArray =new ArrayList <String>();
    String clase,credito,admin="";
    EditText pass,nombre,apellido,correo,genero,fecha,telefono,direccion;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO";
    String password="password";
    String puerto="1521";
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);
        nombre=(EditText) findViewById(R.id.nombre_registro);
        pass=(EditText) findViewById(R.id.clave_registro);
        apellido=(EditText) findViewById(R.id.apellido_registro);
        correo=(EditText) findViewById(R.id.correo_registro);
        genero=(EditText) findViewById(R.id.genero_registro);
        fecha=(EditText) findViewById(R.id.fecha_registro);
        telefono=(EditText) findViewById(R.id.telefono_registro);
        direccion=(EditText) findViewById(R.id.direccion_registro);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
      Bundle  bundle = getIntent().getExtras();
        if (bundle != null) {
            try{
                admin=bundle.getString("Admin");
                UserArray = bundle.getStringArrayList("User");
             }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }
    }
    public void BotonInsertar(View v) {
        if(TodoValido())
        {
            String id=getID();
            logueado.setId(id);
            try {
                String consulta="BEGIN \n";
                consulta+="usuario_android('"+logueado.getClase()+"',"+credito+","+id+",'10/11/2016','"+logueado.getNombre()+"'";
                consulta+=", '"+logueado.getApellido()+"' , '"+logueado.getCorreo()+"','"+logueado.getPass()+"', "+logueado.getTelefono()+",'"+logueado.getDireccion()+"'";
                consulta+=",'"+fecha.getText()+"', '"+genero.getText()+"','"+logueado.getId()+".png');";
                consulta+="\n END;";
                Class.forName(driver).newInstance();
                con = DriverManager.getConnection(url, usuario, password);
                Statement st=con.createStatement();
                st.executeQuery(consulta);
                st.close();
                con.close();
                if(admin.equals("YES"))
                {
                    Toast.makeText(getApplicationContext(),"Usuario Creado Correctamente!",Toast.LENGTH_LONG).show();
                    Intent form = new Intent(Registro.this, Admin.class);
                    form.putExtra("User",UserArray);
                    startActivity(form);
                }
                else{
                Intent form = new Intent(Registro.this, Cliente.class);
                form.putExtra("User",logueado.toArrayString());
                startActivity(form);}

            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }

        }
        else{
            Toast.makeText(getApplicationContext(),"Por favor ingrese todos los campos",Toast.LENGTH_SHORT).show();
        }
    }
    private Boolean TodoValido()
    {
         setClase();
        if(nombre.getText().equals("") ||pass.getText().equals("") ||apellido.getText().equals("") ||correo.getText().equals("") ||genero.getText().equals("") ||fecha.getText().equals("") ||telefono.getText().equals("") ||direccion.getText().equals(""))
        {
            return false;
        }
        else{
            logueado.setClase(clase);
            logueado.setCredito(credito);
            logueado.setNombre(nombre.getText().toString());
            logueado.setPass(pass.getText().toString());
            logueado.setApellido(apellido.getText().toString());
            logueado.setCorreo(correo.getText().toString());
            logueado.setGanancia("0");
            logueado.setTelefono(telefono.getText().toString());
            logueado.setDireccion(direccion.getText().toString());
            return true;
        }

    }
    private String getID()
    {
        String id="";
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery("SELECT seq_usuarios_idusuario.nextval  FROM dual");
            if(resultado.next())
            {
                id=resultado.getString("Nextval");
            }
            resultado.close();
            st.close();
            con.close();

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }

        return id;
    }
    private void setClase()
    {
      int num=(int) (Math.random()*5+1);
        if(num==1)
        {
            clase="A";
            credito="20000";
        }
        else if(num==3)
        {
            clase="B";
            credito="15000";
        }
        else if(num==4)
        {
            clase="C";
            credito="10000";
        }
        else if(num==2)
        {
            clase="D";
            credito="5000";
        }
        else
        {
            clase="E";
            credito="1000";
        }
    }
}
