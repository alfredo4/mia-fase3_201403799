package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by alfredo on 8/11/16.
 */

public class Usuarios extends AppCompatActivity {
    Usuario busqueda= new Usuario();
    private ArrayList<String> lista_Producto =new ArrayList <String>();
    private ArrayList<String> UserArray =new ArrayList <String>();
    private ListView ListaRe;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO"; //Nombre de usuario de la bd
    String password="password";//Password de la bd
    String puerto="1521"; //puerto
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_usuario);
        Bundle bundle = getIntent().getExtras();
        lista_Producto = bundle.getStringArrayList("Usuarios");
        UserArray = bundle.getStringArrayList("User");
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ListaRe=(ListView) findViewById(R.id.lista_u);
        if(lista_Producto!=null)
        {
            ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, lista_Producto);
            ListaRe.setAdapter(adaptador);
            //lista.clear();
            ListaRe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    String[] auxiliar = lista_Producto.get(position).toString().split("\n");
                    BuscarUsuario(auxiliar[0]);
                    Intent form = new Intent(Usuarios.this, Crud_Usuario.class);
                    form.putExtra("User",UserArray);
                    form.putExtra("Busqueda",busqueda.toArrayString());
                    startActivity(form);
                }
            });
        }
    }
    private void BuscarUsuario(String codgio)
    {
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st = con.createStatement();
            ResultSet resultado=st.executeQuery("Select * FROM Usuario WHERE ID_Usuario="+codgio);
            while(resultado.next())
            {
                busqueda.setNombre(resultado.getString("Nombre_Usuario"));
                busqueda.setApellido(resultado.getString("Apellido_Usuario"));
                busqueda.setCorreo(resultado.getString("Correo"));
                busqueda.setTelefono((resultado.getString("Telefono_Usuario")));
                busqueda.setDireccion(resultado.getString("Direccion_Usuario"));
                busqueda.setPass(resultado.getString("Password_Usuario"));
                busqueda.setId(resultado.getString("ID_Usuario"));
                busqueda.setCredito(resultado.getString("Credito"));
                busqueda.setGanancia(resultado.getString("Ganancia_Usuario"));
                busqueda.setClase(resultado.getString("Clase_Cliente"));
            }
            resultado.close();
            st.close();
            con.close();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();

        }
    }
}
