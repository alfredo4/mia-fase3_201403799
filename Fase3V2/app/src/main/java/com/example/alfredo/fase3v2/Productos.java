package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by alfredo on 8/11/16.
 */

public class Productos extends AppCompatActivity {
    Articulo pro= new Articulo();
    private ArrayList<String> lista_Producto =new ArrayList <String>();
    private ArrayList<String> UserArray =new ArrayList <String>();
    private ListView ListaRe;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO"; //Nombre de usuario de la bd
    String password="password";//Password de la bd
    String puerto="1521"; //puerto
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        lista_Producto.clear();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productos);
        Bundle bundle = getIntent().getExtras();
        lista_Producto = bundle.getStringArrayList("Productos");
        if(bundle.getStringArrayList("User")==null)
        {}
        else {
            UserArray = bundle.getStringArrayList("User");
             }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ListaRe=(ListView) findViewById(R.id.lista_p);
        if(lista_Producto!=null)
        {
            ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, lista_Producto);
            ListaRe.setAdapter(adaptador);
            //lista.clear();
            ListaRe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    String[] auxiliar = lista_Producto.get(position).toString().split("\n");
                    BuscarProducto(auxiliar[0]);
                    Intent form = new Intent(Productos.this, Producto.class);
                    form.putExtra("Nombre",pro.getNombre());
                    form.putExtra("Codigo",pro.getCodigo());
                    form.putExtra("Cantidad",pro.getCantidad());
                    form.putExtra("Categoria",pro.getCategoria());
                    form.putExtra("Descripcion",pro.getDescripcion());
                    form.putExtra("Fecha",pro.getFecha());
                    form.putExtra("Precio",pro.getPrecio());
                    form.putExtra("User",UserArray);
                    startActivity(form);
                }
            });
        }
    }
    private void BuscarProducto(String codgio)
    {
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st = con.createStatement();
            ResultSet resultado=st.executeQuery("Select * FROM Producto WHERE Codigo_Producto='"+codgio+"'");
            while(resultado.next())
            {
                pro.setNombre(resultado.getString("Nombre_Producto"));
                pro.setCantidad(resultado.getString("Cantidad"));
                pro.setCategoria(resultado.getString("Categoria_Nombre_Categoria"));
                pro.setCodigo(resultado.getString("Codigo_Producto"));
                pro.setDescripcion(resultado.getString("Descripcion"));
                pro.setFecha(resultado.getString("Fecha_Publicacion"));
                pro.setPrecio(resultado.getString("Precio_Producto"));
            }
            resultado.close();
            st.close();
            con.close();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();

        }
    }
}
