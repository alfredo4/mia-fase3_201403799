package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by alfredo on 8/11/16.
 */

public class Carrito extends AppCompatActivity {
    Articulo pro= new Articulo();
    private ArrayList<String> Lista_Carrito =new ArrayList <String>();
    private ArrayList<String> Lista_Carrito2 =new ArrayList <String>();
    private ArrayList<String> UserArray =new ArrayList <String>();
    Usuario user = new Usuario();
    private ListView ListaRe;
    TextView Total;
    int total=0;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO"; //Nombre de usuario de la bd
    String password="password";//Password de la bd
    String puerto="1521"; //puerto
    String ip="192.168.0.8";
    String tot,id_factura;
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
    protected void onCreate(Bundle savedInstanceState) {
        Lista_Carrito.clear();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.carrito);
        Bundle bundle = getIntent().getExtras();
        Lista_Carrito = bundle.getStringArrayList("Carrito");
        Lista_Carrito2 = bundle.getStringArrayList("Carrito2");
        UserArray = bundle.getStringArrayList("User");
        user.FromArrayString(UserArray);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ListaRe=(ListView) findViewById(R.id.lista_c);
        Total =(TextView) findViewById(R.id.total);
        tot=bundle.getString("Total");
        Total.setText("Total Q."+bundle.getString("Total"));
        if(Lista_Carrito !=null)
        {
            ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, Lista_Carrito);
            ListaRe.setAdapter(adaptador);
            //lista.clear();
            ListaRe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    String[] auxiliar = Lista_Carrito.get(position).toString().split("\n");
                    Intent form = new Intent(Carrito.this, Producto_Carrito.class);
                    form.putExtra("User",UserArray);
                    form.putExtra("Producto",Lista_Carrito2.get(position));
                    startActivity(form);
                }
            });
        }
    }
    private String Propietario_Producto(String codgio)
    {
        String id="0";
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st = con.createStatement();
            ResultSet resultado=st.executeQuery("Select Propietario FROM DETALLE_Producto WHERE Codigo_PD='"+codgio+"'");
            while(resultado.next())
            {
                id=resultado.getString("Propietario");
            }
            resultado.close();
            st.close();
            con.close();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString()+" ACA12",Toast.LENGTH_LONG).show();

        }
        return id;
    }
    public void Comprar_Carrito(View v) {
           double total_carrito=Double.parseDouble(tot);
            double credito=Double.parseDouble(user.getCredito());
            if(total_carrito>credito)
            {
                Toast.makeText(getApplicationContext(),"No tiene suficiente Credito!",Toast.LENGTH_LONG).show();
            }
            else{
                Crear_Factura();
                Llenar_Detalle();
                credito-=total_carrito;
                user.setCredito(String.valueOf(credito));
                Toast.makeText(getApplicationContext(),"Muchas Gracias por su Compra!",Toast.LENGTH_LONG).show();
                Intent form = new Intent(Carrito.this, Cliente.class);
                form.putExtra("User",user.toArrayString());
                startActivity(form);
            }
        }
    private String getID()
    {
        String id="";
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery("SELECT secuencia_factura.nextval FROM dual");
            if(resultado.next())
            {
                id=resultado.getString("Nextval");
            }
            resultado.close();
            st.close();
            con.close();

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString()+" ACA1",Toast.LENGTH_LONG).show();
        }
        return id;
    }
    private void Llenar_Detalle()
    {
        for(int i=0;i<Lista_Carrito.size();i++)
        {
            String[] auxiliar = Lista_Carrito2.get(i).toString().split("-");
            String Consulta="BEGIN\n";
            Consulta+="detalle_android("+getID()+","+auxiliar[4]+","+Propietario_Producto(auxiliar[0])+","+user.getId()+","+auxiliar[2]+","+auxiliar[3]+","+id_factura+",'"+auxiliar[0]+"');";
            Consulta+="\nEND;";
            Ejecutar(Consulta);
        }
    }
    private void Crear_Factura()
    {
        id_factura=getID();
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            st.executeQuery("INSERT INTO FACTURA (ID_Factura,CARRITO_USUARIO_ID_Usuario,Total_Factura) VALUES("+id_factura+","+user.getId()+","+tot+")");
            st.close();
            con.close();

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }

    }
    private void Ejecutar(String Consulta)
    {
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            st.executeQuery(Consulta);
            st.close();
            con.close();

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }

    }
}
