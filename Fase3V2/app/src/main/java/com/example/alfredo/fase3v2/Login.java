package com.example.alfredo.fase3v2;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by alfredo on 7/11/16.
 */

public class Login extends AppCompatActivity {
    Usuario logueado = new Usuario();
    EditText pass;
    EditText id;
    String bd="XE";
    String driver="oracle.jdbc.driver.OracleDriver";
    String usuario="DJANGO";
    String password="password";
    String puerto="1521";
    String ip="192.168.0.8";
    String url="jdbc:oracle:thin:@"+ip+":"+puerto+":"+bd;
    Connection con;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.login);
            id=(EditText) findViewById(R.id.Id_usuario_login);
            pass=(EditText) findViewById(R.id.password_login);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

    public void BotonLogin(View v) {
        try {
            Class.forName(driver).newInstance();
            con = DriverManager.getConnection(url, usuario, password);
            Statement st=con.createStatement();
            ResultSet resultado=st.executeQuery("Select * FROM USUARIO WHERE ID_Usuario="+id.getText()+" AND Password_Usuario='"+pass.getText()+"'");
            if(resultado.next())
            {
                logueado.setNombre(resultado.getString("Nombre_Usuario"));
                logueado.setApellido(resultado.getString("Apellido_Usuario"));
                logueado.setCorreo(resultado.getString("Correo"));
                logueado.setTelefono((resultado.getString("Telefono_Usuario")));
                logueado.setDireccion(resultado.getString("Direccion_Usuario"));
                logueado.setPass(resultado.getString("Password_Usuario"));
                logueado.setId(resultado.getString("ID_Usuario"));
                logueado.setCredito(resultado.getString("Credito"));
                logueado.setGanancia(resultado.getString("Ganancia_Usuario"));
                logueado.setClase(resultado.getString("Clase_Cliente"));

            }
            resultado.close();
            st.close();
            con.close();
            if(logueado.getNombre().equals(""))
            {
                Toast.makeText(getApplicationContext(),"Credenciales no validos",Toast.LENGTH_SHORT).show();
            }
            else
            {
               // Toast.makeText(getApplicationContext(),"Bienvenido "+logueado.getNombre(),Toast.LENGTH_SHORT).show();
                if(logueado.getClase().equals("S"))
                {
                    Intent form = new Intent(Login.this, Admin.class);
                    form.putExtra("User",logueado.toArrayString());
                    startActivity(form);
                }
                else if(logueado.getClase().equals("0"))
                {
                    Toast.makeText(getApplicationContext(),"El usuario no esta habilitado",Toast.LENGTH_SHORT).show();
                }
                else
                {
                Intent form = new Intent(Login.this, Cliente.class);
                form.putExtra("User",logueado.toArrayString());
                startActivity(form);
                }

           }
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }

    }

}
