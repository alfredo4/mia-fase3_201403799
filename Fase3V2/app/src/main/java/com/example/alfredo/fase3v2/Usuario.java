package com.example.alfredo.fase3v2;

import java.util.ArrayList;

/**
 * Created by alfredo on 8/11/16.
 */

public class Usuario {
    String nombre,apellido,direccion,clave,correo,id,pass,ganancia,credito,clase;
    String telefono;

    public Usuario() {
     nombre="";
        apellido="";
        direccion="";
        clave="";
        correo="";
        telefono="";
        pass="";
        id="";
        credito="";
        clase="";
        ganancia="";
    }

    public String getCredito() {
        return credito;
    }

    public void setCredito(String credito) {
        this.credito = credito;
    }

    public String getGanancia() {
        return ganancia;
    }

    public void setGanancia(String ganancia) {
        this.ganancia = ganancia;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<String> toArrayString()
    {
        ArrayList<String> MiUsuario =new ArrayList <String>();
        MiUsuario.add(nombre);
        MiUsuario.add(apellido);
        MiUsuario.add(pass);
        MiUsuario.add(correo);
        MiUsuario.add(telefono);
        MiUsuario.add(direccion);
        MiUsuario.add(credito);
        MiUsuario.add(ganancia);
        MiUsuario.add(id);
        MiUsuario.add(clase);
        return MiUsuario;
    }
    public void FromArrayString( ArrayList<String> MiUsuario)
    {
        nombre=MiUsuario.get(0);
        apellido=MiUsuario.get(1);
        pass=MiUsuario.get(2);
        correo=MiUsuario.get(3);
        telefono=MiUsuario.get(4);
        direccion=MiUsuario.get(5);
        credito=MiUsuario.get(6);
        ganancia=MiUsuario.get(7);
        id=MiUsuario.get(8);
        clase=MiUsuario.get(9);

    }
}
